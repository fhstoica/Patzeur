#ifndef CHECKSANDEVALS_H
#define CHECKSANDEVALS_H

#include "structs.h"

namespace Eval{
  
  bool verifyIfCheck(const Position& init_pos, 
		     unsigned long long square, 
		     unsigned short int turn);
  bool verifyIfDrawn(const Position& init_pos, 
		     unsigned short int turn);
  unsigned long long allAttacks(const Position& init_pos, 
				unsigned short int turn);
  unsigned long long unblockedOpponentAttacks(const Position& init_pos, 
					      unsigned short int turn);
  long int evaluateOneSide(const Position& init_pos,
			   unsigned short int turn,
			   unsigned long long unblocked_opponent_attacks,
			   unsigned long long all_opponent_attacks,
			   unsigned long long allOpponentBlockers);
  long int evaluateOneSideSimple(const Position& init_pos,
				 unsigned short int turn);
  
  long int evaluate(const Position& init_pos, 
		    unsigned short int turn);
}

#endif
