#include "staticDefinitions.h"
#include "movesGen.h"
#include "checksAndEvals.h"
#include "hashing.h"
#include "IO.h"


namespace Moves{
#include "constants.h"
#include "hashing_constants.h"
  
  inline void genRookMoves(const Position& init_pos, 
			   std::list<Position>& legal_moves, 
			   unsigned short int turn, 
			   unsigned long long allOpponentBlockers, 
			   unsigned long long opponentKing, 
			   unsigned long long allOwnBlockers,
			   unsigned long long unblocked_opponent_attacks,
			   bool captures_only){
    unsigned int pos, new_location;
    unsigned long long blockers, rook_moves, captures, mask, new_mask;
    mask = init_pos.player[turn].Rooks;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      blockers = (allOpponentBlockers | opponentKing | allOwnBlockers) & 
	(~(1ULL << pos)); //Exclude the piece itself from the blockers.
      blockers &= rook_masks[pos];
      rook_moves = rook_attacks[pos][(blockers * rook_magics[pos]) >> rook_shifts[pos]];
      rook_moves &= ~(opponentKing | allOwnBlockers);
      captures = rook_moves & allOpponentBlockers;

      if(captures_only){
	new_mask = captures;
      }
      else{
	new_mask = rook_moves;
      }

      while(new_mask != 0){
	Position new_pos(init_pos);
	new_location = __builtin_ctzll(new_mask);
	new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][ROOKS_INDEX][pos];
	new_pos.player[turn].Rooks = ((new_pos.player[turn].Rooks &  (~(1ULL << pos))) | /*Delete the old position*/ 
				      (1ULL << new_location)); /*Set the new position*/
	new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][ROOKS_INDEX][new_location];

	if((captures & new_pos.player[turn].Rooks) != 0ULL){
	  unsigned short int opponent_turn = (1 + turn) % 2;
	  if(new_pos.player[opponent_turn].Pawns & new_pos.player[turn].Rooks){
	    new_pos.player[opponent_turn].Pawns   &= (~new_pos.player[turn].Rooks);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][PAWNS_INDEX][new_location];	    
	  }
	  if(new_pos.player[opponent_turn].Bishops & new_pos.player[turn].Rooks){
	    new_pos.player[opponent_turn].Bishops &= (~new_pos.player[turn].Rooks);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][BISHOPS_INDEX][new_location];
	  }
	  if(new_pos.player[opponent_turn].Knights & new_pos.player[turn].Rooks){
	    new_pos.player[opponent_turn].Knights &= (~new_pos.player[turn].Rooks);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][KNIGHTS_INDEX][new_location];
	  }
	  if(new_pos.player[opponent_turn].Rooks & new_pos.player[turn].Rooks){
	    new_pos.player[opponent_turn].Rooks   &= (~new_pos.player[turn].Rooks);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][ROOKS_INDEX][new_location];
	  }
	  if(new_pos.player[opponent_turn].Queens  & new_pos.player[turn].Rooks){
	    new_pos.player[opponent_turn].Queens  &= (~new_pos.player[turn].Rooks);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][QUEENS_INDEX][new_location];
	  }
	}
	if(new_pos.player[turn].castleS != 0){//Can castle short
	  if((1ULL << pos) && rook_init_pos_castle_short[turn]){ //Rook that moves is the one for short castling H1/H8
	    new_pos.player[turn].castleS = 0; //No longer able to castle short
	    new_pos.Zobrist_hash ^= Hashing::zobrist_castle_vals[turn][0];
	  } 
	}
	if(new_pos.player[turn].castleL != 0){//Can castle long
	  if((1ULL << pos) && rook_init_pos_castle_long[turn]){ //Rook that moves is the one for long castling A1/A8
	    new_pos.player[turn].castleL = 0; //No longer able to castle long
	    new_pos.Zobrist_hash ^= Hashing::zobrist_castle_vals[turn][1];
	  }
	}

	new_pos.en_passant = 0ULL;
	if((unblocked_opponent_attacks & new_pos.player[turn].King) == 0ULL){//Lone king not attacked by enemy pieces, not in check
	  legal_moves.push_back(new_pos);
	}
	else{
	  if(!Eval::verifyIfCheck(new_pos, new_pos.player[turn].King, turn)){//Full verification that king is not in check
	    legal_moves.push_back(new_pos);
	  }
	  else{
	    /*King is in check, discard the position*/
	  }
	}
	new_mask &= (new_mask - 1);
      }
      mask &= (mask - 1);
    }
  }


  inline  void genBishopMoves(const Position& init_pos, 
			      std::list<Position>& legal_moves, 
			      unsigned short int turn, 
			      unsigned long long allOpponentBlockers, 
			      unsigned long long opponentKing, 
			      unsigned long long allOwnBlockers, 
			      unsigned long long unblocked_opponent_attacks,
			      bool captures_only){
    unsigned int pos, new_location;
    unsigned long long blockers, bishop_moves, captures, mask, new_mask;
    mask = init_pos.player[turn].Bishops;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      blockers = (allOpponentBlockers | opponentKing | allOwnBlockers) & 
	(~(1ULL << pos)); //Exclude the piece itself from the blockers.
      blockers &= bishop_masks[pos];
      bishop_moves = bishop_attacks[pos][(blockers * bishop_magics[pos]) >> bishop_shifts[pos]];
      bishop_moves &= ~(opponentKing | allOwnBlockers);
      captures = bishop_moves & allOpponentBlockers;

      if(captures_only){
	new_mask = captures;
      }
      else{
	new_mask = bishop_moves;
      }

      while(new_mask != 0){
	Position new_pos(init_pos);
	new_location = __builtin_ctzll(new_mask);
	new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][BISHOPS_INDEX][pos];
	new_pos.player[turn].Bishops = ((new_pos.player[turn].Bishops &  (~(1ULL << pos))) | /*Delete the old position*/ 
					(1ULL << new_location)); /*Set the new position*/
	new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][BISHOPS_INDEX][new_location];

	if((captures & new_pos.player[turn].Bishops) != 0ULL){
	  unsigned short int opponent_turn = (1 + turn) % 2;
	  if(new_pos.player[opponent_turn].Pawns & new_pos.player[turn].Bishops){
	    new_pos.player[opponent_turn].Pawns   &= (~new_pos.player[turn].Bishops);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][PAWNS_INDEX][new_location];
	  }
	  if(new_pos.player[opponent_turn].Bishops & new_pos.player[turn].Bishops){
	    new_pos.player[opponent_turn].Bishops &= (~new_pos.player[turn].Bishops);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][BISHOPS_INDEX][new_location];
	  }
	  if(new_pos.player[opponent_turn].Knights & new_pos.player[turn].Bishops){
	    new_pos.player[opponent_turn].Knights &= (~new_pos.player[turn].Bishops);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][KNIGHTS_INDEX][new_location];
	  }
	  if(new_pos.player[opponent_turn].Rooks & new_pos.player[turn].Bishops){
	    new_pos.player[opponent_turn].Rooks   &= (~new_pos.player[turn].Bishops);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][ROOKS_INDEX][new_location];
	  }
	  if(new_pos.player[opponent_turn].Queens & new_pos.player[turn].Bishops){
	    new_pos.player[opponent_turn].Queens  &= (~new_pos.player[turn].Bishops);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][QUEENS_INDEX][new_location];
	  }	  
	}

	new_pos.en_passant = 0ULL;
	if((unblocked_opponent_attacks & new_pos.player[turn].King) == 0ULL){//Lone king not attacked by enemy pieces, not in check
	  legal_moves.push_back(new_pos);
	}
	else{
	  if(!Eval::verifyIfCheck(new_pos, new_pos.player[turn].King, turn)){//Full verification that king is not in check
	    legal_moves.push_back(new_pos);
	  }
	  else{
	    /*King is in check, discard the position*/
	  }
	}
	new_mask &= (new_mask - 1);
      }
      mask &= (mask - 1);
    }
  }

  inline  void genQueenMoves(const Position& init_pos, 
			     std::list<Position>& legal_moves, 
			     unsigned short int turn, 
			     unsigned long long allOpponentBlockers, 
			     unsigned long long opponentKing, 
			     unsigned long long allOwnBlockers,
			     unsigned long long unblocked_opponent_attacks,
			     bool captures_only){
    unsigned int pos, new_location;
    unsigned long long blockers, queen_moves, captures, mask, new_mask;
    mask = init_pos.player[turn].Queens;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      blockers = (allOpponentBlockers | opponentKing | allOwnBlockers) & 
	(~(1ULL << pos)); //Exclude the piece itself from the blockers.
      queen_moves = rook_attacks[pos][((blockers & rook_masks[pos]) * rook_magics[pos]) >> rook_shifts[pos]];
      queen_moves |= bishop_attacks[pos][((blockers & bishop_masks[pos]) * bishop_magics[pos]) >> bishop_shifts[pos]];

      queen_moves &= ~(opponentKing | allOwnBlockers);
      captures = queen_moves & allOpponentBlockers;

      if(captures_only){
	new_mask = captures;
      }
      else{
	new_mask = queen_moves;
      }

      while(new_mask != 0){
	Position new_pos(init_pos);
	new_location = __builtin_ctzll(new_mask);
	new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][QUEENS_INDEX][pos];
	new_pos.player[turn].Queens = ((new_pos.player[turn].Queens &  (~(1ULL << pos))) | /*Delete the old position*/ 
				       (1ULL << new_location)); /*Set the new position*/
	new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][QUEENS_INDEX][new_location];

	if((captures & new_pos.player[turn].Queens) != 0ULL){
	  unsigned short int opponent_turn = (1 + turn) % 2;
	  if(new_pos.player[opponent_turn].Pawns & new_pos.player[turn].Queens){
	    new_pos.player[opponent_turn].Pawns   &= (~new_pos.player[turn].Queens);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][PAWNS_INDEX][new_location];
	  }
	  if(new_pos.player[opponent_turn].Bishops & new_pos.player[turn].Queens){
	    new_pos.player[opponent_turn].Bishops &= (~new_pos.player[turn].Queens);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][BISHOPS_INDEX][new_location];
	  }
	  if(new_pos.player[opponent_turn].Knights & new_pos.player[turn].Queens){
	    new_pos.player[opponent_turn].Knights &= (~new_pos.player[turn].Queens);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][KNIGHTS_INDEX][new_location];
	  }
	  if(new_pos.player[opponent_turn].Rooks & new_pos.player[turn].Queens){
	    new_pos.player[opponent_turn].Rooks   &= (~new_pos.player[turn].Queens);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][ROOKS_INDEX][new_location];
	  }
	  if(new_pos.player[opponent_turn].Queens & new_pos.player[turn].Queens){
	    new_pos.player[opponent_turn].Queens  &= (~new_pos.player[turn].Queens);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][QUEENS_INDEX][new_location];
	  } 
	}

	new_pos.en_passant = 0ULL;
	if((unblocked_opponent_attacks & new_pos.player[turn].King) == 0ULL){//Lone king not attacked by enemy pieces, not in check
	  legal_moves.push_back(new_pos);
	}
	else{
	  if(!Eval::verifyIfCheck(new_pos, new_pos.player[turn].King, turn)){//Full verification that king is not in check
	    legal_moves.push_back(new_pos);
	  }
	  else{
	    /*King is in check, discard the position*/
	  }
	}
	new_mask &= (new_mask - 1);
      }
      mask &= (mask - 1);
    }
  }

 inline void genKnightMoves(const Position& init_pos, 
			    std::list<Position>& legal_moves, 
			    unsigned short int turn, 
			    unsigned long long allOpponentBlockers, 
			    unsigned long long opponentKing, 
			    unsigned long long allOwnBlockers, 
			    unsigned long long unblocked_opponent_attacks,
			    bool captures_only){
   unsigned int pos, new_location;
   unsigned long long blockers, knight_moves, captures, mask, new_mask;
   mask = init_pos.player[turn].Knights;
   while(mask != 0){
     pos = __builtin_ctzll(mask);
     blockers = (allOpponentBlockers | opponentKing | allOwnBlockers) & 
       (~(1ULL << pos)); //Exclude the piece itself from the blockers.
     blockers &= knight_attacks[pos];
     knight_moves = knight_attacks[pos];
     knight_moves &= ~(opponentKing | allOwnBlockers);
     captures = knight_moves & allOpponentBlockers;

     if(captures_only){
	new_mask = captures;
      }
      else{
	new_mask = knight_moves;
      }

     while(new_mask != 0){
       Position new_pos(init_pos);
       new_location = __builtin_ctzll(new_mask);
       new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][KNIGHTS_INDEX][pos];
       new_pos.player[turn].Knights = ((new_pos.player[turn].Knights &  (~(1ULL << pos))) | /*Delete the old position*/ 
				       (1ULL << new_location)); /*Set the new position*/
       new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][KNIGHTS_INDEX][new_location];

       if((captures & new_pos.player[turn].Knights) != 0ULL){
	 unsigned short int opponent_turn = (1 + turn) % 2;
	 if(new_pos.player[opponent_turn].Pawns & new_pos.player[turn].Knights){
	   new_pos.player[opponent_turn].Pawns   &= (~new_pos.player[turn].Knights);
	   new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][PAWNS_INDEX][new_location];
	 }
	 if(new_pos.player[opponent_turn].Bishops & new_pos.player[turn].Knights){
	   new_pos.player[opponent_turn].Bishops &= (~new_pos.player[turn].Knights);
	   new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][BISHOPS_INDEX][new_location];
	 }
	 if(new_pos.player[opponent_turn].Knights & new_pos.player[turn].Knights){
	   new_pos.player[opponent_turn].Knights &= (~new_pos.player[turn].Knights);
	   new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][KNIGHTS_INDEX][new_location];
	 }
	 if(new_pos.player[opponent_turn].Rooks & new_pos.player[turn].Knights){
	   new_pos.player[opponent_turn].Rooks   &= (~new_pos.player[turn].Knights);
	   new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][ROOKS_INDEX][new_location];
	 }
	 if(new_pos.player[opponent_turn].Queens & new_pos.player[turn].Knights){
	   new_pos.player[opponent_turn].Queens  &= (~new_pos.player[turn].Knights);
	   new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][QUEENS_INDEX][new_location];
	 }
       }

       new_pos.en_passant = 0ULL;
       if((unblocked_opponent_attacks & new_pos.player[turn].King) == 0ULL){//Lone king not attacked by enemy pieces, not in check
	 legal_moves.push_back(new_pos);
       }
       else{
	 if(!Eval::verifyIfCheck(new_pos, new_pos.player[turn].King, turn)){//Full verification that king is not in check
	   legal_moves.push_back(new_pos);
	 }
	 else{
	   /*King is in check, discard the position*/
	 }
       }
       new_mask &= (new_mask - 1);
     }
     mask &= (mask - 1);
   }
 }

  inline void genKingMoves(const Position& init_pos, 
			   std::list<Position>& legal_moves, 
			   unsigned short int turn, 
			   unsigned long long allOpponentBlockers, 
			   unsigned long long opponentKing, 
			   unsigned long long allOwnBlockers, 
			   unsigned long long unblocked_opponent_attacks,
			   bool captures_only){
    unsigned int pos, new_location;
    unsigned long long blockers, king_moves, captures, mask, new_mask;
    mask = init_pos.player[turn].King;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      blockers = (allOpponentBlockers | opponentKing | allOwnBlockers) & 
	(~(1ULL << pos)); //Exclude the piece itself from the blockers.
      blockers &= king_attacks[pos];
      king_moves = king_attacks[pos];
      king_moves &= ~(opponentKing | allOwnBlockers);
      captures = king_moves & allOpponentBlockers;

      if(captures_only){
	new_mask = captures;
      }
      else{
	new_mask = king_moves;
      }

      while(new_mask != 0){
	Position new_pos(init_pos);
	new_location = __builtin_ctzll(new_mask);
	new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][KING_INDEX][pos];
	new_pos.player[turn].King = ((new_pos.player[turn].King &  (~(1ULL << pos))) | /*Delete the old position*/ 
				     (1ULL << new_location)); /*Set the new position*/
	new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][KING_INDEX][new_location];

	if((captures & new_pos.player[turn].King) != 0ULL){
	  unsigned short int opponent_turn = (1 + turn) % 2;
	  if(new_pos.player[opponent_turn].Pawns & new_pos.player[turn].King){
	    new_pos.player[opponent_turn].Pawns   &= (~new_pos.player[turn].King);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][PAWNS_INDEX][new_location];
	  }
	  if(new_pos.player[opponent_turn].Bishops & new_pos.player[turn].King){
	    new_pos.player[opponent_turn].Bishops &= (~new_pos.player[turn].King);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][BISHOPS_INDEX][new_location];
	  }
	  if(new_pos.player[opponent_turn].Knights & new_pos.player[turn].King){
	    new_pos.player[opponent_turn].Knights &= (~new_pos.player[turn].King);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][KNIGHTS_INDEX][new_location];
	  }
	  if(new_pos.player[opponent_turn].Rooks & new_pos.player[turn].King){
	    new_pos.player[opponent_turn].Rooks   &= (~new_pos.player[turn].King);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][ROOKS_INDEX][new_location];
	  }
	  if(new_pos.player[opponent_turn].Queens & new_pos.player[turn].King){
	    new_pos.player[opponent_turn].Queens  &= (~new_pos.player[turn].King);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][QUEENS_INDEX][new_location];
	  }
	}

	new_pos.en_passant = 0ULL;
	if((new_pos.player[turn].King & unblocked_opponent_attacks) == 0){//King not attacked by any opponent pieces, not in check
	  if(new_pos.player[turn].castleS != 0){
	    new_pos.player[turn].castleS = 0;
	    new_pos.Zobrist_hash ^= Hashing::zobrist_castle_vals[turn][0];
	  }
	  if(new_pos.player[turn].castleL != 0){
	    new_pos.player[turn].castleL = 0;
	    new_pos.Zobrist_hash ^= Hashing::zobrist_castle_vals[turn][1];
	  }
	  legal_moves.push_back(new_pos);
	}
	else{
	  if(!Eval::verifyIfCheck(new_pos, new_pos.player[turn].King, turn)){//Verify that the king is not in check
	    if(new_pos.player[turn].castleS != 0){
	      new_pos.player[turn].castleS = 0;
	      new_pos.Zobrist_hash ^= Hashing::zobrist_castle_vals[turn][0];
	    }
	    if(new_pos.player[turn].castleL != 0){
	      new_pos.player[turn].castleL = 0;
	      new_pos.Zobrist_hash ^= Hashing::zobrist_castle_vals[turn][1];
	    }
	    legal_moves.push_back(new_pos);
	  }
	}
	new_mask &= (new_mask - 1);
      }
      mask &= (mask - 1);
    }
  }

  inline void enPassantCapture(const Position& init_pos, 
			       std::list<Position>& legal_moves, 
			       unsigned long long unblocked_opponent_attacks,
			       unsigned short int turn){
    
    unsigned short int opponent_turn = (1 + turn) % 2;    
    unsigned long long can_capture_en_passant = (init_pos.player[turn].Pawns & 
						 pawn_capture_attacks[opponent_turn][__builtin_ctzll(init_pos.en_passant)]);

    if(can_capture_en_passant != 0ULL){
      unsigned int pos, new_location;
      unsigned long long mask;//, new_mask;
      while(can_capture_en_passant != 0){
	pos = __builtin_ctzll(can_capture_en_passant);
	Position new_pos(init_pos);
	new_pos.en_passant = 0ULL;
	new_pos.player[turn].Pawns = ((new_pos.player[turn].Pawns &  (~(1ULL << pos))) | init_pos.en_passant);
	new_pos.player[opponent_turn].Pawns &= (~((turn == 0) ? (init_pos.en_passant << 8) : (init_pos.en_passant >> 8))); 
	new_pos.en_passant = 0ULL; //The new position inherits the value from init_pos and that is non-zero.

	mask = init_pos.player[turn].Pawns ^ new_pos.player[turn].Pawns; //Gives the old and new location of the moved pawn
	while(mask != 0){
	  new_location = __builtin_ctzll(mask);
	  new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][PAWNS_INDEX][new_location];
	  mask &= (mask - 1);
	}

	mask = init_pos.player[opponent_turn].Pawns ^ new_pos.player[opponent_turn].Pawns; //Gives the location of the captures opponent pawn
	while(mask != 0){ //There should be only one iteration of this loop
	  new_location = __builtin_ctzll(mask);
	  new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][PAWNS_INDEX][new_location];
	  mask &= (mask - 1);
	}


	if((unblocked_opponent_attacks & new_pos.player[turn].King) == 0ULL){//Lone king not attacked by enemy pieces, not in check
	  legal_moves.push_back(new_pos);
	}
	else{
	  if(!Eval::verifyIfCheck(new_pos, new_pos.player[turn].King, turn)){//Full verification that king is not in check
	    legal_moves.push_back(new_pos);
	  }
	  else{
	    /*King is in check, discard the position*/
	  }
	}
	can_capture_en_passant &= (can_capture_en_passant - 1);
      }
    }
    //init_pos.en_passant = 0ULL; //Reset the en-passant square for the INITIAL_POSITION, such that the new positions will have it set to zero. 
  }

  inline void pawnPromotion(const Position& init_pos,
			    std::list<Position>& legal_moves,
			    unsigned short int turn){
    unsigned long long promotion_square = promotion_rank[turn] & init_pos.player[turn].Pawns;
    unsigned int new_location = __builtin_ctzll(promotion_square);
    
    Position promote_bishop(init_pos);
    promote_bishop.player[turn].Pawns   &= (~promotion_square);
    promote_bishop.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][PAWNS_INDEX][new_location];
    promote_bishop.player[turn].Bishops |=   promotion_square;
    promote_bishop.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][BISHOPS_INDEX][new_location];
    legal_moves.push_back(promote_bishop);
  
    Position promote_knight(init_pos);
    promote_knight.player[turn].Pawns   &= (~promotion_square);
    promote_knight.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][PAWNS_INDEX][new_location];
    promote_knight.player[turn].Knights |=   promotion_square;
    promote_knight.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][KNIGHTS_INDEX][new_location];
    legal_moves.push_back(promote_knight);
  
    Position promote_rook(init_pos);
    promote_rook.player[turn].Pawns     &= (~promotion_square);
    promote_rook.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][PAWNS_INDEX][new_location];
    promote_rook.player[turn].Rooks     |=   promotion_square;
    promote_rook.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][ROOKS_INDEX][new_location];
    legal_moves.push_back(promote_rook);
  
    Position promote_queen(init_pos);
    promote_queen.player[turn].Pawns    &= (~promotion_square);
    promote_queen.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][PAWNS_INDEX][new_location];
    promote_queen.player[turn].Queens   |=   promotion_square;
    promote_queen.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][QUEENS_INDEX][new_location];
    legal_moves.push_back(promote_queen);
  }

  inline void genPawnMoves(const Position& init_pos, 
			   std::list<Position>& legal_moves, 
			   unsigned short int turn, 
			   unsigned long long allOpponentBlockers, 
			   unsigned long long opponentKing, 
			   unsigned long long allOwnBlockers, 
			   unsigned long long unblocked_opponent_attacks,
			   bool captures_only){
    unsigned int pos, new_c_pos;
    unsigned long long blockers, pawn_moves, captures, mask, new_mask;
    enPassantCapture(init_pos, legal_moves, unblocked_opponent_attacks, turn);
    mask = init_pos.player[turn].Pawns;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      blockers = ((allOpponentBlockers | 
		   opponentKing | 
		   allOwnBlockers) & 
		  (~(1ULL << pos))); //Exclude the piece itself from the blockers.

      blockers   &= pawn_attacks[turn][pos]; //Keep only blockers that overlap with the actual moves
      pawn_moves  = blockers & pawn_attacks[turn][pos];
      pawn_moves  = (pawn_moves ^ pawn_attacks[turn][pos]) & ((turn == 0) ? ~(blockers >> 8) : ~(blockers << 8)); 
      captures    = pawn_capture_attacks[turn][pos] & allOpponentBlockers;

      if(captures_only){
	new_mask = captures;
      }
      else{
	new_mask = pawn_moves | captures;
      }

      while(new_mask != 0){
	new_c_pos = __builtin_ctzll(new_mask);
	Position new_pos(init_pos);
	new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][PAWNS_INDEX][pos];
	new_pos.en_passant = 0ULL; //Reset the en_passant square by default. Set it back later if needed.
	new_pos.player[turn].Pawns = ((new_pos.player[turn].Pawns &  (~(1ULL << pos))) | /*Delete the old position*/ 
				      (1ULL << new_c_pos)); /*Set the new position*/
	new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[turn][PAWNS_INDEX][new_c_pos];

	if((captures & new_pos.player[turn].Pawns) != 0ULL){
	  unsigned short int opponent_turn = (1 + turn) % 2;
	  if(new_pos.player[opponent_turn].Pawns & new_pos.player[turn].Pawns){
	    new_pos.player[opponent_turn].Pawns   &= (~new_pos.player[turn].Pawns);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][PAWNS_INDEX][new_c_pos];
	  }
	  if(new_pos.player[opponent_turn].Bishops & new_pos.player[turn].Pawns){
	    new_pos.player[opponent_turn].Bishops &= (~new_pos.player[turn].Pawns);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][BISHOPS_INDEX][new_c_pos];
	  }
	  if(new_pos.player[opponent_turn].Knights & new_pos.player[turn].Pawns){
	    new_pos.player[opponent_turn].Knights &= (~new_pos.player[turn].Pawns);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][KNIGHTS_INDEX][new_c_pos];
	  }
	  if(new_pos.player[opponent_turn].Rooks & new_pos.player[turn].Pawns){
	    new_pos.player[opponent_turn].Rooks   &= (~new_pos.player[turn].Pawns);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][ROOKS_INDEX][new_c_pos];
	  }
	  if(new_pos.player[opponent_turn].Queens & new_pos.player[turn].Pawns){
	    new_pos.player[opponent_turn].Queens  &= (~new_pos.player[turn].Pawns);
	    new_pos.Zobrist_hash ^= Hashing::zobrist_piece_vals[opponent_turn][QUEENS_INDEX][new_c_pos];
	  }
	}
	//Check if pawn moves two squares and can be captured en passant
	if(turn == 0){
	  if(((1ULL << pos) >> 16 ) & (1ULL << new_c_pos)){
	    new_pos.en_passant = (1ULL << pos) >> 8;
	  }
	  else{
	    new_pos.en_passant = 0ULL;
	  }
	}
	else{
	  if(((1ULL << pos) << 16 ) & (1ULL << new_c_pos)){
	    new_pos.en_passant = (1ULL << pos) << 8;
	  }
	  else{
	    new_pos.en_passant = 0ULL;
	  }
	}
	/************** First check if we need to promote a pawn **************/
	if(new_pos.player[turn].Pawns & promotion_rank[turn]){
	  if((unblocked_opponent_attacks & new_pos.player[turn].King) == 0ULL){//Lone king not attacked by enemy pieces, not in check
	    pawnPromotion(new_pos, legal_moves, turn);
	  }
	  else{
	    if(!Eval::verifyIfCheck(new_pos, new_pos.player[turn].King, turn)){//Full verification that king is not in check
	      pawnPromotion(new_pos, legal_moves, turn);
	    }
	    else{
	      /*King is in check, discard the position*/
	    }
	  }
	}
	/************** If no promotion is made, simply add the pawn move **************/
	else{
	  if((unblocked_opponent_attacks & new_pos.player[turn].King) == 0ULL){//Lone king not attacked by enemy pieces, not in check
	    legal_moves.push_back(new_pos);
	  }
	  else{
	    if(!Eval::verifyIfCheck(new_pos, new_pos.player[turn].King, turn)){//Full verification that king is not in check
	      legal_moves.push_back(new_pos);
	    }
	    else{
	      /*King is in check, discard the position*/
	    }
	  }
	}
	/*******************************************************************************/
	new_mask &= (new_mask - 1);
      }
      mask &= (mask - 1);
    }
  }

  inline void generateCastle(const Position& init_pos,
			     std::list<Position>& legal_moves,
			     unsigned short int turn,
			     unsigned long long allOpponentBlockers,
			     unsigned long long unblocked_opponent_attacks,
			     unsigned long long allOwnBlockers){
    
    if(init_pos.player[turn].castleS){

      if((castle_short[turn] & //the corresponding short_castle_space is the same as spaces the king moves = castle_short.
	  (allOpponentBlockers | 
	   (allOwnBlockers & 
	    (~init_pos.player[turn].King)))) == 0ULL && /*No pieces in king's or rook's way*/
	 ((init_pos.player[turn].Rooks & rook_init_pos_castle_short[turn]) != 0ULL) && //The rook is in the H1/H8 square
	 ((init_pos.player[turn].King & king_init_pos_castle[turn]) != 0ULL)){//The king is on the E1/E8 square
	
	/*We don't check the king position, as we remove castling the moment it is moved.*/
	if((unblocked_opponent_attacks & castle_short[turn]) == 0ULL/*Lone king on the board not attacked by any oppent pieces. Not in check*/){
	  Position new_pos(init_pos);
	  new_pos.player[turn].Rooks &= (~rook_init_pos_castle_short[turn]); //Remvoe rook from old position
	  new_pos.player[turn].Rooks |=  rook_final_pos_castle_short[turn];  //Add rook in final position
	  new_pos.player[turn].King = (new_pos.player[turn].King << 2); //Move the king 2 squares to the right
	  
	  new_pos.player[turn].castleS = 0;
	  new_pos.player[turn].castleL = 0;
	  new_pos.Zobrist_hash  = ::Hashing::computeHash(new_pos);
	  legal_moves.push_back(new_pos);
	}
	else{ //Do a full verification if the king is in check
	  if((!Eval::verifyIfCheck(init_pos, castle_short[turn], turn))){ //None of the king passing squares are in check.
	    Position new_pos(init_pos);
	    new_pos.player[turn].Rooks &= (~rook_init_pos_castle_short[turn]); //Remvoe rook from old position
	    new_pos.player[turn].Rooks |=  rook_final_pos_castle_short[turn];  //Add rook in final position
	    new_pos.player[turn].King = (new_pos.player[turn].King << 2); //Move the king 2 squares to the right
	    
	    new_pos.player[turn].castleS = 0;
	    new_pos.player[turn].castleL = 0;
	    new_pos.Zobrist_hash  = ::Hashing::computeHash(new_pos);
	    legal_moves.push_back(new_pos);
	  }
	}
      }
    }
    if(init_pos.player[turn].castleL){
      
      if((long_castle_space[turn] & 
	  (allOpponentBlockers | 
	   (allOwnBlockers & 
	    (~init_pos.player[turn].King)))) == 0ULL && /*No pieces in king's or rook's way*/
	 ((init_pos.player[turn].Rooks & rook_init_pos_castle_long[turn]) != 0ULL) && //The rook is in the A1/A8 square
	 ((init_pos.player[turn].King & king_init_pos_castle[turn]) != 0ULL)){//The king is on the E1/E8 square
	
	/*We don't check the king position, as we remove castling the moment it is moved.*/
	if((unblocked_opponent_attacks & castle_long[turn]) == 0ULL/*Lone king on the board not attacked by any oppent pieces. Not in check*/){
	  Position new_pos(init_pos);
	  new_pos.player[turn].Rooks &= (~rook_init_pos_castle_long[turn]); //Remvoe rook from old position
	  new_pos.player[turn].Rooks |=  rook_final_pos_castle_long[turn];  //Add rook in final position
	  new_pos.player[turn].King = (new_pos.player[turn].King >> 2); //Move the king 2 squares to the left
	 
	  new_pos.player[turn].castleS = 0;
	  new_pos.player[turn].castleL = 0;
	  new_pos.Zobrist_hash  = ::Hashing::computeHash(new_pos);
	  legal_moves.push_back(new_pos);
	}
	else{ //Do a full verification if the king is in check
	  if((!Eval::verifyIfCheck(init_pos, castle_long[turn], turn))){ //None of the king passing squares are in check.
	    Position new_pos(init_pos);
	    new_pos.player[turn].Rooks &= (~rook_init_pos_castle_long[turn]); //Remvoe rook from old position
	    new_pos.player[turn].Rooks |=  rook_final_pos_castle_long[turn];  //Add rook in final position
	    new_pos.player[turn].King = (new_pos.player[turn].King >> 2); //Move the king 2 squares to the left
	    
	    new_pos.player[turn].castleS = 0;
	    new_pos.player[turn].castleL = 0;
	    new_pos.Zobrist_hash  = ::Hashing::computeHash(new_pos);
	    legal_moves.push_back(new_pos);
	  }
	}
      }
      
      
    }
  }

  void generateAllMoves(const Position& init_pos,
			std::list<Position>& legal_moves,
			unsigned short int turn, 
			bool captures_only){
    //turn = 0, black, turn = 1 white
  
    unsigned long long allOpponentBlockers; //Excludes king, which cannot be captured
    unsigned long long allOwnBlockers;
    unsigned short int opponent_turn = (1 + turn) % 2;
    unsigned long long unblocked_opp_atk = Eval::unblockedOpponentAttacks(init_pos, turn);
    
    allOpponentBlockers = (init_pos.player[opponent_turn].Pawns | 
			   init_pos.player[opponent_turn].Bishops | 
			   init_pos.player[opponent_turn].Knights | 
			   init_pos.player[opponent_turn].Rooks | 
			   init_pos.player[opponent_turn].King | 
			   init_pos.player[opponent_turn].Queens);
  
    //init_pos.player[opponent_turn].King;

    allOwnBlockers = (init_pos.player[turn].Pawns | 
		      init_pos.player[turn].Bishops | 
		      init_pos.player[turn].Knights | 
		      init_pos.player[turn].Rooks | 
		      init_pos.player[turn].King | 
		      init_pos.player[turn].Queens);

    if(!captures_only){
      generateCastle(init_pos, legal_moves, turn, allOpponentBlockers, unblocked_opp_atk, allOwnBlockers);
    }
    /**/
    genRookMoves(init_pos, legal_moves, turn, allOpponentBlockers, init_pos.player[opponent_turn].King, allOwnBlockers, unblocked_opp_atk, captures_only);
    genBishopMoves(init_pos, legal_moves, turn, allOpponentBlockers, init_pos.player[opponent_turn].King, allOwnBlockers, unblocked_opp_atk, captures_only);
    genQueenMoves(init_pos, legal_moves, turn, allOpponentBlockers, init_pos.player[opponent_turn].King, allOwnBlockers, unblocked_opp_atk, captures_only);
    genKnightMoves(init_pos, legal_moves, turn, allOpponentBlockers, init_pos.player[opponent_turn].King, allOwnBlockers, unblocked_opp_atk, captures_only);
    genKingMoves(init_pos, legal_moves, turn, allOpponentBlockers, init_pos.player[opponent_turn].King, allOwnBlockers, unblocked_opp_atk, captures_only);
    genPawnMoves(init_pos, legal_moves, turn, allOpponentBlockers, init_pos.player[opponent_turn].King, allOwnBlockers, unblocked_opp_atk, captures_only);
    /**/
  }
}
