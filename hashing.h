#ifndef HASHING_H
#define HASHING_H

#include "structs.h"

namespace Hashing{
  typedef struct{
    long int score;
    unsigned int depth;
    unsigned int move_no;
    Position position;
  } HashVal;

  unsigned long long computeHash(const Position& init_pos);
  void writePerThreadHashTable(const Position& init_pos, 
			       unsigned int thread_no, 
			       long int score, 
			       unsigned int depth, 
			       unsigned int move_no);
  bool retrieveCachedScore(const Position& pos,
			   unsigned int depth, 
			   long int& cached_score);
  void initHashTable(void);
  void purgeHashTable(unsigned int move_no);
  void combineHashTables(void);
  void outputHashTable(void);
  void initZobristPieceVals(void);
  bool isEqual(const Position& a, 
	       const Position& b);
}
#endif
