#include <pthread.h>
#include <unistd.h>
#include <stdexcept>
#include <algorithm>
#include <map>
#include <utility>
#include <boost/lexical_cast.hpp>
#include "treeSearch.h"
#include "IO.h"
#include "utils.h"
#include "hashing.h"
#include "checksAndEvals.h"
#include "MultiThreadedDispatcherTemplate.hpp"

namespace Search{
#include "staticDefinitions.h"
 
  void setInitialPosition(Position& init_pos){
    init_pos.player[0].Pawns   = ((1ULL << 8) - 1ULL) << 48;
    init_pos.player[0].Rooks   = (1ULL << 56) | (1ULL << 63);
    init_pos.player[0].Knights = (1ULL << 57) | (1ULL << 62);
    init_pos.player[0].Bishops = (1ULL << 58) | (1ULL << 61);
    init_pos.player[0].Queens  = (1ULL << 59);
    init_pos.player[0].King    = (1ULL << 60);
    
    init_pos.player[0].castleL = 1;
    init_pos.player[0].castleS = 1;

    init_pos.player[1].Pawns   = ((1ULL << 8) - 1ULL) <<   8;
    init_pos.player[1].Rooks   = 1ULL | (1ULL << 7);
    init_pos.player[1].Knights = (1ULL << 1) | (1ULL << 6);
    init_pos.player[1].Bishops = (1ULL << 2) | (1ULL << 5);
    init_pos.player[1].Queens  = (1ULL << 3);
    init_pos.player[1].King    = (1ULL << 4);
    
    init_pos.player[1].castleL = 1;
    init_pos.player[1].castleS = 1;

    init_pos.en_passant        = 0ULL;
    init_pos.Zobrist_hash      = Hashing::computeHash(init_pos);
  }

  template<typename T> void processData(unsigned int thread_no, T* data);
  template<> void processData(unsigned int thread_no, std::pair<long int, TopLevelData>* data){
    unsigned int max_depth = 0;
    unsigned long long counter = 0ULL;
    unsigned long long transp_table_count = 0ULL;

    TopLevelData td = data->second;
    td.thread_no = thread_no;
    long int score = alphaBetaMin(td.position,
				  td.moves_played,
				  td.thread_no,
				  td.turn,
				  td.original_turn,
				  td.max_positions,
				  td.quiesence_depth, 
				  td.captures_only,
				  td.depth,
				  max_depth,
				  counter,
				  transp_table_count,
				  td.move_no,
				  -CHECKMATE_VALUE,
				  CHECKMATE_VALUE);

    data->second.max_depth = max_depth;
    data->second.counter = counter;
    data->second.transp_table_count = transp_table_count;
    data->first = score;
  }

  void topLevelSearchBestPosMT(const Position& init_pos,
			       Position& final_pos,
			       std::list<Position>& moves_played,
			       unsigned int turn,
			       const unsigned int& original_turn,
			       unsigned long long max_positions,
			       unsigned int& max_depth,
			       int quiesence_depth, 
			       bool captures_only, 
			       unsigned long long& counter,
			       unsigned long long& transp_table_count,
			       unsigned int move_no){
    
    std::list<Position> legal_moves;
    Moves::generateAllMoves(init_pos, legal_moves, turn, captures_only);
    
    std::list<TopLevelData> top_level_position_data;
    for(std::list<Position>::iterator it = legal_moves.begin(); it != legal_moves.end(); ++it){
      TopLevelData tp;
      tp.position           = *it;
      std::copy(moves_played.begin(), moves_played.end(), std::back_inserter(tp.moves_played));
      tp.thread_no          = 0;
      tp.score              = 0L;
      tp.turn               = (1+turn)%2; 
      tp.original_turn      = original_turn;
      tp.max_positions      = max_positions;
      tp.depth              = 0;
      tp.max_depth          = 0;
      tp.quiesence_depth    = quiesence_depth;
      tp.captures_only      = captures_only;
      tp.counter            = 0ULL;
      tp.transp_table_count = 0ULL;
      tp.move_no            = move_no;
      top_level_position_data.push_back(tp);
    }
    
    if(legal_moves.empty()){
      std::cout << "Check, mate !" << std::endl;
    }
    
    MultiThreadedDispatcherClass< std::pair<long int, TopLevelData> >::DataToProcess data;
    for(std::list<TopLevelData>::iterator it_td = top_level_position_data.begin();
	it_td != top_level_position_data.end();
	++it_td){
      long int score = 0ULL;
      std::pair<long int, TopLevelData> currPosAndScore(score, *it_td);
      data.input_list.push_back(currPosAndScore);
    }
    MultiThreadedDispatcherClass<std::pair<long int, TopLevelData>> mtdc(&data, processData, NO_OF_CPUS);
    mtdc.multiThreadedDispatcher();
    Hashing::combineHashTables();

    std::multimap<long int, TopLevelData> positionsAndScores;

    unsigned long long total_counter  = 0ULL;
    unsigned int       total_depth    = 0;
    unsigned long long total_transp_table_count  = 0ULL;
    for(std::list<std::pair<long int, TopLevelData> >::iterator mm_tt = data.output_list.begin();
	mm_tt != data.output_list.end();
	++mm_tt){
      positionsAndScores.insert(*mm_tt);
      total_counter            += mm_tt->second.counter;
      total_transp_table_count += mm_tt->second.transp_table_count;
      if(total_depth < mm_tt->second.max_depth){
	total_depth = mm_tt->second.max_depth;
      }
    }
    final_pos          = positionsAndScores.rbegin()->second.position;
    max_depth          = total_depth;
    counter            = total_counter;
    transp_table_count = total_transp_table_count;
  }
  
  //////////////////////////////////////////// alpha-beta pruning ////////////////////////////////////////////

  long int alphaBetaMax(const Position& init_pos,
			std::list<Position>& moves_played,
			unsigned int thread_no,
			unsigned int turn,
			const unsigned int& original_turn,
			unsigned long long max_positions,
			unsigned int quiesence_depth, 
			bool captures_only,
			unsigned int depth,
			unsigned int& max_depth,
			unsigned long long& counter,
			unsigned long long& transp_table_count,
			unsigned int move_no,
			long int alpha,
			long int beta){
    long int positionScore;
    if(/* MAX_DEPTH <= depth */ max_positions == 0ULL ){
      ++counter;
      if(max_depth < depth){
	max_depth = depth;
      }
      positionScore = Eval::evaluate(init_pos, original_turn);
      return(positionScore);
    }
    else{
      std::list<Position> legal_moves;
      if(depth <= quiesence_depth){ 
	Moves::generateAllMoves(init_pos, legal_moves, turn, captures_only);
      }
      else{
	Moves::generateAllMoves(init_pos, legal_moves, turn, true);
      }
      
      if(legal_moves.empty()){//There are no legal moves left from this position
	if(Eval::verifyIfCheck(init_pos, init_pos.player[turn].King, turn)){//Found a checkmate
	  if(turn == original_turn){
	    return(CHECKMATE_VALUE/(depth+1));//Lost, have been checkmated
	  }
	  else{
	    return(CHECKMATE_VALUE/(depth+1));//Won, checkmated the opponent
	  }
	}
	else{
	  return(0L); //Stalemate
	}
      }
      else{//There are still legal moves left
	std::multimap<long int, Position> sorted_legal_moves;
	for(std::list<Position>::iterator it = legal_moves.begin(); it != legal_moves.end(); ++it){
	  if(Hashing::retrieveCachedScore(*it, depth, positionScore)){
	    sorted_legal_moves.insert(std::pair<long int, Position>(positionScore, *it));
	  }
	  else{
	    sorted_legal_moves.insert(std::pair<long int, Position>(Eval::evaluate(*it, original_turn), *it));
	  }
	}
	for(std::multimap<long int, Position>::reverse_iterator mm_tt = sorted_legal_moves.rbegin();
	    mm_tt != sorted_legal_moves.rend();
	    ++mm_tt){
	  Position* it = &(mm_tt->second);
	  /****************************
	  moves_played.push_back(*it);
	  
	  if(Utils::checkThreefoldRepetition(moves_played)){//Three-fold repetition, draw
	    moves_played.pop_back();
	    transp_table_count += 1;
	    return(0L); 
	  }
	  ******************************/
	  if(Hashing::retrieveCachedScore(*it, depth, positionScore)){//Position found in transposition table
	    transp_table_count += 1;
	  }
	  else{
	    //////////////////// Now go down one level ////////////////////
	    positionScore = alphaBetaMin(*it,
					 moves_played,
					 thread_no,
					 (1 + turn)%2,
					 original_turn,
					 (unsigned long long) max_positions/legal_moves.size(),
					 quiesence_depth, 
					 captures_only,
					 depth + 1,
					 max_depth,
					 counter,
					 transp_table_count,
					 move_no,
					 alpha,
					 beta);
	    
	    Hashing::writePerThreadHashTable(*it, thread_no, positionScore, depth, move_no);
	  }
	  //moves_played.pop_back();
	  //Maximize the score
	  if(beta <= positionScore){
	    return(beta);
	  }
	  if(alpha < positionScore){
	    alpha = positionScore;
	  }
	}
      }
    }
    return(alpha);
  }

  long int alphaBetaMin(const Position& init_pos,
			std::list<Position>& moves_played,
			unsigned int thread_no,
			unsigned int turn,
			const unsigned int& original_turn,
			unsigned long long max_positions,
			unsigned int quiesence_depth, 
			bool captures_only,
			unsigned int depth,
			unsigned int& max_depth,
			unsigned long long& counter,
			unsigned long long& transp_table_count,
			unsigned int move_no,
			long int alpha,
			long int beta){
    long int positionScore;
    if(/* MAX_DEPTH <= depth */ max_positions == 0ULL ){
      ++counter;
      if(max_depth < depth){
	max_depth = depth;
      }
      positionScore = Eval::evaluate(init_pos, original_turn);
      return(positionScore);
    }
    else{
      std::list<Position> legal_moves;
      if(depth <= quiesence_depth){ 
	Moves::generateAllMoves(init_pos, legal_moves, turn, captures_only);
      }
      else{
	Moves::generateAllMoves(init_pos, legal_moves, turn, true);
      }
      
      if(legal_moves.empty()){//There are no legal moves left from this position
	if(Eval::verifyIfCheck(init_pos, init_pos.player[turn].King, turn)){//Found a checkmate
	  if(turn == original_turn){
	    return(CHECKMATE_VALUE/(depth+1));//Lost, have been checkmated
	  }
	  else{
	    return(CHECKMATE_VALUE/(depth+1));//Won, checkmated the opponent
	  }
	}
	else{
	  return(0L); //Stalemate
	}
      }
      else{//There are still legal moves left
	std::multimap<long int, Position> sorted_legal_moves;
	for(std::list<Position>::iterator it = legal_moves.begin(); it != legal_moves.end(); ++it){
	  if(Hashing::retrieveCachedScore(*it, depth, positionScore)){
	    sorted_legal_moves.insert(std::pair<long int, Position>(positionScore, *it));
	  }
	  else{
	    sorted_legal_moves.insert(std::pair<long int, Position>(Eval::evaluate(*it, original_turn), *it));
	  }
	}
	for(std::multimap<long int, Position>::iterator mm_tt = sorted_legal_moves.begin();
	    mm_tt != sorted_legal_moves.end();
	    ++mm_tt){
	  Position* it = &(mm_tt->second);
	  /***************************
	  moves_played.push_back(*it);
	  
	  if(Utils::checkThreefoldRepetition(moves_played)){//Three-fold repetition, draw
	    moves_played.pop_back();
	    transp_table_count += 1;
	    return(0L); 
	  }
	  ***************************/
	  if(Hashing::retrieveCachedScore(*it, depth, positionScore)){//Position found in transposition table
	    transp_table_count += 1;
	  }
	  else{
	    //////////////////// Now go down one level ////////////////////
	    positionScore = alphaBetaMax(*it,
					 moves_played,
					 thread_no,
					 (1 + turn)%2,
					 original_turn,
					 (unsigned long long) max_positions/legal_moves.size(),
					 quiesence_depth, 
					 captures_only,
					 depth + 1,
					 max_depth,
					 counter,
					 transp_table_count,
					 move_no,
					 alpha,
					 beta);
	    
	    Hashing::writePerThreadHashTable(*it, thread_no, positionScore, depth, move_no);
	  }
	  //moves_played.pop_back();
	  //Minimize the score	  
	  if(positionScore <= alpha){
	    return(alpha);
	  }
	  if(positionScore < beta){
	    beta = positionScore;
	  }
	}
      }
    }
    return(beta);
  }
}
