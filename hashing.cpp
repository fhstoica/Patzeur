#include <iostream>
#include <random>
#include <limits>
#include "hashing_constants.h"
#include "hashing.h"
#include "IO.h"
#include "treeSearch.h"

namespace Hashing{
#include "staticDefinitions.h"

  

  const long long HASH_TABLE_SIZE = HASH_TABLE_STATIC_SIZE; 
  const long long FULL_HASH_TABLE_SIZE = NO_OF_CPUS * HASH_TABLE_SIZE;
  HashVal hash_table[FULL_HASH_TABLE_SIZE];
  HashVal per_thread_hash_table[NO_OF_CPUS][HASH_TABLE_SIZE];
  
  
  void outputHashTable(void){
    for(unsigned int i = 0; i < FULL_HASH_TABLE_SIZE; ++i){
      
      std::cout << "hash : 0x" << std::hex << hash_table[i].position.Zobrist_hash
		<< " depth : " << std::dec << hash_table[i].depth 
		<< " score : " << hash_table[i].score 
		<< " move_no : " << hash_table[i].move_no << std::endl;
      
    }
    return;
  }

  void initHashTable(void){
    for(unsigned int i = 0; i < FULL_HASH_TABLE_SIZE; ++i){
      Search::setInitialPosition(hash_table[i].position);
      hash_table[i].depth                 = 1000;
      hash_table[i].score                 = 0L;
      hash_table[i].move_no               = 0; 
      hash_table[i].position.Zobrist_hash = 0ULL;
    }
    for(unsigned int cpu = 0; cpu < NO_OF_CPUS; ++cpu){
      for(unsigned int j = 0; j < HASH_TABLE_SIZE; ++j){
	Search::setInitialPosition(per_thread_hash_table[cpu][j].position);
	per_thread_hash_table[cpu][j].position.Zobrist_hash = 0ULL;
	per_thread_hash_table[cpu][j].depth                 = 1000;
	per_thread_hash_table[cpu][j].score                 = 0L;
	per_thread_hash_table[cpu][j].move_no               = 0;
      }
    }
  }
  
  void purgeHashTable(unsigned int move_no){
    //unsigned long long counter_T = 0;
    //unsigned long long counter_A = 0;
    for(unsigned int cpu = 0; cpu < NO_OF_CPUS; ++cpu){
      for(unsigned int j = 0; j < HASH_TABLE_SIZE; ++j){
	if(MOVES_TO_KEEP < (move_no - per_thread_hash_table[cpu][j].move_no)){
	  Search::setInitialPosition(per_thread_hash_table[cpu][j].position);
	  per_thread_hash_table[cpu][j].position.Zobrist_hash = 0ULL;
	  per_thread_hash_table[cpu][j].depth                 = MAX_DEPTH;
	  per_thread_hash_table[cpu][j].score                 = 0L;
	  per_thread_hash_table[cpu][j].move_no               = 0;
	  // ++counter_T;
	}
	if(MOVES_TO_KEEP < (move_no - hash_table[cpu * HASH_TABLE_SIZE + j].move_no)){
	  Search::setInitialPosition(hash_table[cpu * HASH_TABLE_SIZE + j].position);
	  hash_table[cpu * HASH_TABLE_SIZE + j].position.Zobrist_hash = 0ULL;
	  hash_table[cpu * HASH_TABLE_SIZE + j].depth                 = MAX_DEPTH;
	  hash_table[cpu * HASH_TABLE_SIZE + j].score                 = 0L;
	  hash_table[cpu * HASH_TABLE_SIZE + j].move_no               = 0;
	  // ++counter_A;
	}
      }
    }
    //std::cout << "purgeHashTable() : per_tread purged = " << counter_T << " combined purged = " << counter_A << std::endl;
  }

  void writePerThreadHashTable(const Position& pos, unsigned int thread_no, long int score, unsigned int depth, unsigned int move_no){
    unsigned long long hash_index = pos.Zobrist_hash % HASH_TABLE_SIZE;
    if(depth <= per_thread_hash_table[thread_no][hash_index].depth){
      //std::cout << " storing position : existing depth = " << per_thread_hash_table[thread_no][hash_index].depth << " new depth = " << depth << std::endl;
      per_thread_hash_table[thread_no][hash_index].position = pos;
      per_thread_hash_table[thread_no][hash_index].score    = score;
      per_thread_hash_table[thread_no][hash_index].depth    = depth;
      per_thread_hash_table[thread_no][hash_index].move_no  = move_no;
    }
  }

  bool retrieveCachedScore(const Position& pos, unsigned int depth, long int& cached_score){
    unsigned long long hash_index = pos.Zobrist_hash % FULL_HASH_TABLE_SIZE;
    if((hash_table[hash_index].position.Zobrist_hash != 0) && (hash_table[hash_index].depth <= depth)){
      if(isEqual(pos, hash_table[hash_index].position)){
	cached_score = hash_table[hash_index].score;
	return(true);	
      }
    }
    return(false);
  }

  void combineHashTables(void){
    //unsigned long long counter    = 0ULL;
    unsigned long long hash_index = 0ULL; 
    for(unsigned int cpu = 0; cpu < NO_OF_CPUS; ++cpu){
      for(unsigned int j = 0; j < HASH_TABLE_SIZE; ++j){
	hash_index = per_thread_hash_table[cpu][j].position.Zobrist_hash % FULL_HASH_TABLE_SIZE;
	hash_table[hash_index] = per_thread_hash_table[cpu][j];
	if(hash_table[hash_index].position.Zobrist_hash != 0ULL){
	  //counter += 1;
	}
      }
    }
    //std::cout << " combineHashTables() : counter = " << counter << std::endl;
  }
  
  unsigned long long computeHash(const Position& init_pos){
    unsigned long long hash = 0ULL;
    unsigned long long mask = 0ULL;
    unsigned short int pos  = 0;
    
    for(unsigned int turn = 0; turn < 2; ++turn){
      if(init_pos.player[turn].castleS != 0){ //0 castleS, 1 castleL
	hash ^= zobrist_castle_vals[turn][0];
      }
      if(init_pos.player[turn].castleL != 0){ //0 castleS, 1 castleL
	hash ^= zobrist_castle_vals[turn][1];
      }
      
      mask = init_pos.player[turn].Pawns;  // Pawns index = 0 
      while(mask != 0){
	pos = __builtin_ctzll(mask);
	hash ^= zobrist_piece_vals[turn][PAWNS_INDEX][pos];
	mask &= (mask - 1);
      }

      mask = init_pos.player[turn].Bishops;  // Bishops index = 1 
      while(mask != 0){
	pos = __builtin_ctzll(mask);
	hash ^= zobrist_piece_vals[turn][BISHOPS_INDEX][pos];
	mask &= (mask - 1);
      }
      
      mask = init_pos.player[turn].Knights;  // Knights index = 2 
      while(mask != 0){
	pos = __builtin_ctzll(mask);
	hash ^= zobrist_piece_vals[turn][KNIGHTS_INDEX][pos];
	mask &= (mask - 1);
      }

      mask = init_pos.player[turn].Rooks;  // Rooks index = 3 
      while(mask != 0){
	pos = __builtin_ctzll(mask);
	hash ^= zobrist_piece_vals[turn][ROOKS_INDEX][pos];
	mask &= (mask - 1);
      }

      mask = init_pos.player[turn].Queens;  // Queens index = 4 
      while(mask != 0){
	pos = __builtin_ctzll(mask);
	hash ^= zobrist_piece_vals[turn][QUEENS_INDEX][pos];
	mask &= (mask - 1);
      }

      mask = init_pos.player[turn].King;  // King index = 5 
      pos = __builtin_ctzll(mask);
      hash ^= zobrist_piece_vals[turn][KING_INDEX][pos];
      
    }
    return(hash);
  }

  bool isEqual(const Position& a, const Position& b){
    if(a.en_passant != b.en_passant){return(false);}
    
    if(a.player[0].castleL != b.player[0].castleL){return(false);}
    if(a.player[0].castleS != b.player[0].castleS){return(false);}

    if(a.player[0].Pawns   != b.player[0].Pawns){  return(false);}
    if(a.player[0].Bishops != b.player[0].Bishops){return(false);}
    if(a.player[0].Knights != b.player[0].Knights){return(false);}
    if(a.player[0].Rooks   != b.player[0].Rooks){  return(false);}
    if(a.player[0].Queens  != b.player[0].Queens){ return(false);}
    if(a.player[0].King    != b.player[0].King){   return(false);}
    
    if(a.player[1].castleL != b.player[1].castleL){return(false);}
    if(a.player[1].castleS != b.player[1].castleS){return(false);}

    if(a.player[1].Pawns   != b.player[1].Pawns){  return(false);}
    if(a.player[1].Bishops != b.player[1].Bishops){return(false);}
    if(a.player[1].Knights != b.player[1].Knights){return(false);}
    if(a.player[1].Rooks   != b.player[1].Rooks){  return(false);}
    if(a.player[1].Queens  != b.player[1].Queens){ return(false);}
    if(a.player[1].King    != b.player[1].King){   return(false);}
    
    return(true);
  }
  
  void initZobristPieceVals(void){
    std::random_device rd;     //Get a random seed from the OS entropy device, or whatever
    std::mt19937_64 eng(rd()); //Use the 64-bit Mersenne Twister 19937 generator and seed it with entropy.
    
    //Define the distribution, by default it goes from 0 to MAX(unsigned long long)
    //or what have you.
    std::uniform_int_distribution<unsigned long long> distr;
    unsigned long long foo;


    for(unsigned int i = 0; i < 2; ++i){
      for(unsigned int j = 0; j < 2; ++j){
	foo = distr(eng) & distr(eng); //& distr(eng);
	zobrist_castle_vals[i][j] = foo;
      }

      for(unsigned int j = 0; j < 6; ++j){
	for(unsigned int k = 0; k < 64; ++k){
	  foo = distr(eng) & distr(eng); //& distr(eng);
	  zobrist_piece_vals[i][j][k] = foo;
	}
      }
    }

  }
}
