#include <stdlib.h>
#include <set>
#include <vector>
#include <boost/lexical_cast.hpp>
#include "IO.h"
#include "movesGen.h"
#include "keyboardInput.h"
#include "utils.h"
#include "hashing.h"

namespace IO{
  void Tokenize(const std::string& str, std::vector<std::string>& tokens, const std::string& delimiters = " "){
    std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    std::string::size_type pos     = str.find_first_of(delimiters, lastPos);
  
    while (std::string::npos != pos || std::string::npos != lastPos){
      tokens.push_back(str.substr(lastPos, pos - lastPos));
      lastPos = str.find_first_not_of(delimiters, pos);
      pos = str.find_first_of(delimiters, lastPos);
    }
  }

  void resetPosition(Position& current_pos){
    current_pos.player[1].Pawns   = 0ULL;
    current_pos.player[1].Bishops = 0ULL;
    current_pos.player[1].Knights = 0ULL;
    current_pos.player[1].Rooks   = 0ULL;
    current_pos.player[1].King    = 0ULL;
    current_pos.player[1].Queens  = 0ULL;
    current_pos.player[1].castleS = 1;
    current_pos.player[1].castleL = 1;
  
    current_pos.player[0].Pawns   = 0ULL;
    current_pos.player[0].Bishops = 0ULL;
    current_pos.player[0].Knights = 0ULL;
    current_pos.player[0].Rooks   = 0ULL;
    current_pos.player[0].King    = 0ULL;
    current_pos.player[0].Queens  = 0ULL;
    current_pos.player[0].castleS = 1;
    current_pos.player[0].castleL = 1;

    current_pos.en_passant        = 0ULL;
    current_pos.Zobrist_hash      = 0ULL;
  }

  void readPosition(Position& pos, std::string file_name){
    resetPosition(pos);
    std::ifstream f_in;
    std::vector<std::string> line_elem;
    std::string c_line;
    unsigned int rank, file;
    f_in.open(file_name.c_str());
    rank = 7;
    while(f_in){
      getline(f_in, c_line);
      line_elem.clear();
      Tokenize(c_line, line_elem);
      rank = atoi(line_elem[0].c_str()) - 1;
      for(unsigned int i = 0; i < line_elem.size(); ++i){
	char pos_elem = line_elem[i][0];
	if((0 <= rank) && (rank <= 7) && 1 <= i){
	  file = i;
	  switch(pos_elem){
	  case 'P':
	    pos.player[1].Pawns |= (1ULL << (8 * rank + file - 1));
	    break;
	  case 'B':
	    pos.player[1].Bishops |= (1ULL << (8 * rank + file - 1));
	    break;
	  case 'N':
	    pos.player[1].Knights |= (1ULL << (8 * rank + file - 1));
	    break;
	  case 'R':
	    pos.player[1].Rooks |= (1ULL << (8 * rank + file - 1));
	    break;
	  case 'K':
	    pos.player[1].King |= (1ULL << (8 * rank + file - 1));
	    break;
	  case 'Q':
	    pos.player[1].Queens |= (1ULL << (8 * rank + file - 1));
	    break;
	  case 'p':
	    pos.player[0].Pawns |= (1ULL << (8 * rank + file - 1));
	    break;
	  case 'b':
	    pos.player[0].Bishops |= (1ULL << (8 * rank + file - 1));
	    break;
	  case 'n':
	    pos.player[0].Knights |= (1ULL << (8 * rank + file - 1));
	    break;
	  case 'r':
	    pos.player[0].Rooks |= (1ULL << (8 * rank + file - 1));
	    break;
	  case 'k':
	    pos.player[0].King |= (1ULL << (8 * rank + file - 1));
	    break;
	  case 'q':
	    pos.player[0].Queens |= (1ULL << (8 * rank + file - 1));
	    break;
	  default:
	    break;
	  }
	}
      }
    }
    pos.Zobrist_hash = Hashing::computeHash(pos); 
  }

  void putPieces(const unsigned long long& piece_bit_board, char piece_symbol, char (&char_board)[8][8]){
    unsigned int rank, file;
    std::vector<unsigned int> piece_vec;
    Utils::getSetBits(piece_bit_board, piece_vec);
    for(std::vector<unsigned int>::const_iterator it = piece_vec.begin(); it != piece_vec.end(); ++it){
      rank = (unsigned int) ((*it)/8);
      file = (unsigned int) ((*it)%8);
      char_board[rank][file] = piece_symbol;
    }
  }

  void printPosition(const Position& pos, std::ostream& f_out){
    char char_board[8][8] = {
      {'.', '.', '.', '.', '.', '.', '.', '.'},
      {'.', '.', '.', '.', '.', '.', '.', '.'}, 
      {'.', '.', '.', '.', '.', '.', '.', '.'},
      {'.', '.', '.', '.', '.', '.', '.', '.'}, 
      {'.', '.', '.', '.', '.', '.', '.', '.'},
      {'.', '.', '.', '.', '.', '.', '.', '.'}, 
      {'.', '.', '.', '.', '.', '.', '.', '.'},
      {'.', '.', '.', '.', '.', '.', '.', '.'}};
  
    putPieces(pos.player[1].Pawns,   'P', char_board);
    putPieces(pos.player[1].Bishops, 'B', char_board);
    putPieces(pos.player[1].Knights, 'N', char_board);
    putPieces(pos.player[1].Rooks,   'R', char_board);
    putPieces(pos.player[1].King,    'K', char_board);
    putPieces(pos.player[1].Queens,  'Q', char_board);

    putPieces(pos.player[0].Pawns,   'p', char_board);
    putPieces(pos.player[0].Bishops, 'b', char_board);
    putPieces(pos.player[0].Knights, 'n', char_board);
    putPieces(pos.player[0].Rooks,   'r', char_board);
    putPieces(pos.player[0].King,    'k', char_board);
    putPieces(pos.player[0].Queens,  'q', char_board);

    for(int rank = 7; 0 <= rank; rank--){
      f_out << rank + 1 << " ";
      for(unsigned int file = 0; file < 8; ++file){
	f_out << char_board[rank][file] << " ";
      } 
      f_out << std::endl;
    }
    f_out << std::endl;
    f_out << "  A B C D E F G H " << std::endl;
  }

  void printPositionColor(const Position& pos, std::ostream& f_out){
    char char_board[8][8] = {
      {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
      {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '}, 
      {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
      {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '}, 
      {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
      {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '}, 
      {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
      {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '}};
  
    putPieces(pos.player[1].Pawns,   'P', char_board);
    putPieces(pos.player[1].Bishops, 'B', char_board);
    putPieces(pos.player[1].Knights, 'N', char_board);
    putPieces(pos.player[1].Rooks,   'R', char_board);
    putPieces(pos.player[1].King,    'K', char_board);
    putPieces(pos.player[1].Queens,  'Q', char_board);

    putPieces(pos.player[0].Pawns,   'p', char_board);
    putPieces(pos.player[0].Bishops, 'b', char_board);
    putPieces(pos.player[0].Knights, 'n', char_board);
    putPieces(pos.player[0].Rooks,   'r', char_board);
    putPieces(pos.player[0].King,    'k', char_board);
    putPieces(pos.player[0].Queens,  'q', char_board);

    for(int rank = 7; 0 <= rank; rank--){
      f_out << rank + 1 << " ";
      for(unsigned int file = 0; file < 8; ++file){
	if((rank + file)%2 == 0){
	  f_out << "\033[1;44m" << char_board[rank][file] << " \033[0m";
	}
	else{
	  f_out << "\033[1;45m" << char_board[rank][file] << " \033[0m";
	}
      } 
      f_out << std::endl;
    }
    f_out << std::endl;
    f_out << "  A B C D E F G H " << std::endl;
  }

  std::string toString(const Position& pos){
    std::string s("");
    char char_board[8][8] = {
      {'.', '.', '.', '.', '.', '.', '.', '.'},
      {'.', '.', '.', '.', '.', '.', '.', '.'}, 
      {'.', '.', '.', '.', '.', '.', '.', '.'},
      {'.', '.', '.', '.', '.', '.', '.', '.'}, 
      {'.', '.', '.', '.', '.', '.', '.', '.'},
      {'.', '.', '.', '.', '.', '.', '.', '.'}, 
      {'.', '.', '.', '.', '.', '.', '.', '.'},
      {'.', '.', '.', '.', '.', '.', '.', '.'}};
  
    putPieces(pos.player[1].Pawns,   'P', char_board);
    putPieces(pos.player[1].Bishops, 'B', char_board);
    putPieces(pos.player[1].Knights, 'N', char_board);
    putPieces(pos.player[1].Rooks,   'R', char_board);
    putPieces(pos.player[1].King,    'K', char_board);
    putPieces(pos.player[1].Queens,  'Q', char_board);
  
    putPieces(pos.player[0].Pawns,   'p', char_board);
    putPieces(pos.player[0].Bishops, 'b', char_board);
    putPieces(pos.player[0].Knights, 'n', char_board);
    putPieces(pos.player[0].Rooks,   'r', char_board);
    putPieces(pos.player[0].King,    'k', char_board);
    putPieces(pos.player[0].Queens,  'q', char_board);

    for(int rank = 0; rank < 8; ++rank){
      for(unsigned int file = 0; file < 8; ++file){
	s.push_back(char_board[rank][file]);
      } 
    }
    return(s);
  }

  std::string diffPositions(const Position& start_pos, const Position& end_pos){

    std::string diff_pos("");
    std::string start_pos_str = toString(start_pos);
    std::string end_pos_str   = toString(end_pos);
  
    std::vector<std::string> files {"A", "B", "C", "D", "E", "F", "G", "H"};

    std::vector<unsigned int> diff_idx_vec;
    for(unsigned int i = 0; i < start_pos_str.size(); ++i){
      if(start_pos_str[i] != end_pos_str[i]){
	diff_idx_vec.push_back(i);
      }
    }

    unsigned int start_idx  = 0;
    unsigned int end_idx    = 0;

    unsigned int start_rank = 0;
    unsigned int start_file = 0;
    unsigned int end_rank   = 0;
    unsigned int end_file   = 0;
    
    if(diff_idx_vec.size() == 2){//Regular move
      if((start_pos_str[diff_idx_vec[0]] != '.') && (start_pos_str[diff_idx_vec[1]] != '.')){//Capture
	if(end_pos_str[diff_idx_vec[0]] != '.'){//Piece captured at diff_idx_vec[0]
	  start_idx = diff_idx_vec[1];
	  end_idx   = diff_idx_vec[0];
	}
	else{
	  if(end_pos_str[diff_idx_vec[1]] != '.'){
	    start_idx = diff_idx_vec[0];
	    end_idx   = diff_idx_vec[1];
	  }
	}
      }
      else{
	if((start_pos_str[diff_idx_vec[0]] != '.') && (start_pos_str[diff_idx_vec[1]] == '.')){//move from  diff_idx_vec[0] --> diff_idx_vec[1]
	  start_idx = diff_idx_vec[0];
	  end_idx   = diff_idx_vec[1];
	}
	else{
	  if((start_pos_str[diff_idx_vec[0]] == '.') && (start_pos_str[diff_idx_vec[1]] != '.')){//move from  diff_idx_vec[1] --> diff_idx_vec[0]
	    start_idx = diff_idx_vec[1];
	    end_idx   = diff_idx_vec[0];
	  }
	}
      }

      start_rank = start_idx/8;
      start_file = start_idx%8;
      end_rank   =   end_idx/8;
      end_file   =   end_idx%8;

      diff_pos = (files[start_file] + boost::lexical_cast<std::string>(start_rank + 1) 
		  + " --> " + 
		  files[end_file] + boost::lexical_cast<std::string>(end_rank + 1) 
		  + " " + 
		  end_pos_str[end_idx] + files[end_file] + boost::lexical_cast<std::string>(end_rank + 1));

      return(diff_pos);
    }
    if(diff_idx_vec.size() == 3){//En passant capture
      char capturing_pawn = ' ';
      for(unsigned int i = 0; i < diff_idx_vec.size(); ++i){
	if(start_pos_str[diff_idx_vec[i]] != '.'){
	  capturing_pawn = start_pos_str[diff_idx_vec[i]];
	  start_rank = diff_idx_vec[i]/8;
	  start_file = diff_idx_vec[i]%8;
	  break;
	}
      }
      for(unsigned int i = 0; i < diff_idx_vec.size(); ++i){
	if(end_pos_str[diff_idx_vec[i]] == capturing_pawn){
	  end_rank   =   diff_idx_vec[i]/8;
	  end_file   =   diff_idx_vec[i]%8;
	  break;
	}
      }

      diff_pos = ("En Passant : " + files[start_file] + boost::lexical_cast<std::string>(start_rank + 1) 
		  + " --> " + 
		  files[end_file] + boost::lexical_cast<std::string>(end_rank + 1) 
		  + " " + 
		  end_pos_str[end_idx] + files[end_file] + boost::lexical_cast<std::string>(end_rank + 1));
      return(diff_pos);
    }
    if(diff_idx_vec.size() == 4){//Castled
      unsigned int white_king_square_00  = 6;
      unsigned int white_king_square_000 = 2;
      unsigned int black_king_square_00  = 62;
      unsigned int black_king_square_000 = 58;
    
      if(end_pos_str[white_king_square_00] == 'K' && start_pos_str[white_king_square_00] == '.'){
	diff_pos = "White 0-0";
      }
      if(end_pos_str[white_king_square_000] == 'K' && start_pos_str[white_king_square_000] == '.'){
	diff_pos = "White 0-0-0";
      }
      if(end_pos_str[black_king_square_00] == 'k' && start_pos_str[black_king_square_00] == '.'){
	diff_pos = "Black 0-0";
      }
      if(end_pos_str[black_king_square_000] == 'k' && start_pos_str[black_king_square_000] == '.'){
	diff_pos = "Black 0-0-0";
      }

      return(diff_pos);
    }
    return(diff_pos);
  }

  bool readMoveFromKeyboard(std::list<Position>& played_moves, Position& pos, unsigned short int turn){
    std::string input_move;
    std::cin >> input_move;
  
    std::list<Position> legal_moves;
    Moves::generateAllMoves(pos, legal_moves, turn, false);
    std::set<std::string> legal_moves_set;
    for(std::list<Position>::iterator it = legal_moves.begin(); it != legal_moves.end(); ++it){
      legal_moves_set.insert(toString(*it));
    }

    if(legal_moves.empty()){
      std::cout << "Check, mate!" << std::endl;
    }
  
    if("x" == input_move){
      std::ofstream f_out;
      f_out.open("moves.log");
      for(std::list<Position>::const_iterator c_itt = played_moves.begin(); c_itt != played_moves.end(); ++c_itt){
	f_out << toString(*c_itt) << std::endl;
      }
      f_out.close();
      exit(0);
    }
  
    if("back" == input_move){
      if(played_moves.size() < 2){
	std::cout << "Cannot go back, " << played_moves.size() << " moves played" << std::endl;
      }
      else{
	played_moves.pop_back();
	played_moves.pop_back();
	pos = played_moves.back();
      }
      return(false);
    }
  
    if("0-0" == input_move){
      Position new_pos(pos);
      KeyboardInput::generateCastleShortKeyboardInput(new_pos, turn);
      if(legal_moves_set.find(toString(new_pos)) != legal_moves_set.end()){
	new_pos.Zobrist_hash = Hashing::computeHash(new_pos);
	pos = new_pos;
	played_moves.push_back(pos);
	return(true);
      }
      else{
	std::cout << "Cannot castle short" << std::endl;
	return(false);
      }
    }
  
    if("0-0-0" == input_move){
      Position new_pos(pos);
      KeyboardInput::generateCastleLongKeyboardInput(new_pos, turn);
      if(legal_moves_set.find(toString(new_pos)) != legal_moves_set.end()){
	new_pos.Zobrist_hash = Hashing::computeHash(new_pos);
	pos = new_pos;
	played_moves.push_back(pos);
	return(true);
      }
      else{
	std::cout << "Cannot castle long" << std::endl;
	return(false);
      }
    }

    if(input_move.size() != 5){
      std::cout << "Moves must be input as startSquare-endSquare (e.g. e2-e4)" << std::endl;
    }
    else{
      char first_file = 'a';
      char first_rank = '1';
      int start_file = input_move[0] - first_file;
      int start_rank = input_move[1] - first_rank;

      int end_file   = input_move[3] - first_file;
      int end_rank   = input_move[4] - first_rank;
      
      if(!((0 <= start_file) && (start_file <= 7) && (0 <= end_file) && (end_file <= 7))){
	std::cout << "Cannot understand input" << std::endl;
	return(false);
      }
      if(!((0 <= start_rank) && (start_rank <= 7) && (0 <= end_rank) && (end_rank <= 7))){
	std::cout << "Cannot understand input" << std::endl;
	return(false);
      }
       
      unsigned long long start_square = 1ULL << (8*start_rank + start_file);
      unsigned long long end_square   = 1ULL << (8*end_rank   + end_file);
 
      Position backup_pos(pos);
      KeyboardInput::movePieceKeyboardInput(pos, turn, start_square, end_square);
     
      if(legal_moves_set.find(toString(pos)) != legal_moves_set.end()){
	pos.Zobrist_hash = Hashing::computeHash(pos);
	played_moves.push_back(pos);
	return(true);
      }
      else{
	pos = backup_pos; //Restore the original position
	std::cout << "Illegal move" << std::endl;
	return(false);
      }
    }
  
    std::cout << "Reached function return without parsing" << std::endl;
    return(false);
  }
}
