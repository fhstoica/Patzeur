GCC = g++ 
GLINK = g++
GLINKFLAGS = -O3 -Wall
GSL = /usr/local/
INCLUDE   = -I$(GSL)include
LIBDIR =  -L$(GSL)lib
LIB64  = -L/usr/lib64
LIB = -lpthread 
GCCFLAGS = -c -O3 -std=c++11 -Wall

all:patzeur.srl

%.srl:patzeur.o utils.o IO.o movesGen.o checksAndEvals.o treeSearch.o hashing.o keyboardInput.o
	$(GLINK) $(GLINKFLAGS) -o $@ $^ $(LIBDIR) $(LIB) $(LIB64)

.PRECIOUS:%.o
	#prevent the deletion of the .o files

%.o:%.cpp
	$(GCC) $(GCCFLAGS) $< $(INCLUDE) 

clean:
	rm -f *~ *.srl *.stackdump *.o

