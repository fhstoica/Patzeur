#ifndef TREE_SEARCH_H
#define TREE_SEARCH_H

#include <list>
#include <utility>
#include <string>
#include "structs.h"
#include "movesGen.h"
#include "checksAndEvals.h"

namespace Search{
  typedef struct{
    unsigned short int thread_no;
    Position           position;
    std::list<Position> moves_played;
    long int           score;
    unsigned int       turn;
    unsigned int       original_turn;
    unsigned long long max_positions;
    unsigned int       depth;
    unsigned int       max_depth;
    unsigned int       quiesence_depth; 
    bool               captures_only; 
    unsigned long long counter;
    unsigned long long transp_table_count;
    unsigned int       move_no;
  } TopLevelData;

  void setInitialPosition(Position& init_pos);
  void topLevelSearchBestPosMT(const Position& init_pos,
			       Position& final_pos,
			       std::list<Position>& moves_played,
			       unsigned int turn,
			       const unsigned int& original_turn,
			       unsigned long long max_positions,
			       unsigned int& max_depth,
			       int quiesence_depth, 
			       bool captures_only, 
			       unsigned long long& counter,
			       unsigned long long& transp_table_count,
			       unsigned int move_no);
  long int alphaBetaMax(const Position& init_pos,
			std::list<Position>& moves_played,
			unsigned int thread_no,
			unsigned int turn,
			const unsigned int& original_turn,
			unsigned long long max_positions,
			unsigned int quiesence_depth, 
			bool captures_only,
			unsigned int depth,
			unsigned int& max_depth,
			unsigned long long& counter,
			unsigned long long& transp_table_count,
			unsigned int move_no,
			long int alpha,
			long int beta);
  long int alphaBetaMin(const Position& init_pos,
			std::list<Position>& moves_played,
			unsigned int thread_no,
			unsigned int turn,
			const unsigned int& original_turn,
			unsigned long long max_positions,
			unsigned int quiesence_depth, 
			bool captures_only,
			unsigned int depth,
			unsigned int& max_depth,
			unsigned long long& counter,
			unsigned long long& transp_table_count,
			unsigned int move_no,
			long int alpha,
			long int beta);
}
#endif
