#ifndef UTILS_H
#define UTILS_H

#include <inttypes.h>
#include <string>
#include <vector>
#include <cstdlib>
#include <list>

namespace Utils{
  bool checkThreefoldRepetition(const std::list<Position>& played_moves);
  std::string printBitBoard(unsigned long long board);
  /* unsigned long long genRookAttackTableEntry(unsigned int pos,  */
  /* 					     unsigned long long occ); */
  /* unsigned long long genBishopAttackTableEntry(unsigned int pos,  */
  /* 					       unsigned long long occ); */
  void getSetBits(unsigned long long mask, 
		  std::vector<unsigned int>& idxs);
  void genOccupancies(std::vector<unsigned int>& idxs, 
		      std::vector<unsigned long long>& occs);
}
#endif
