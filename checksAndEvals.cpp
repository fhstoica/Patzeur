#include "checksAndEvals.h"
#include "pieceValues.h"
#include "utils.h"

#ifdef DEBUG_EVAL
#include <iostream>
#endif

namespace Eval{
#include "constants.h"
#include "staticDefinitions.h"
  
  bool verifyIfCheck(const Position& init_pos,
		     unsigned long long square,
		     unsigned short int turn){
    
    unsigned short int opponent_turn = (turn + 1) % 2;
    unsigned long long mask = 0ULL;
    /*** Verify that kings are not next to each other ***/
    unsigned int opponent_king_pos = __builtin_ctzll(init_pos.player[opponent_turn].King);
    if(king_attacks[opponent_king_pos] & square){return(true);}
    /*** Verify that it is not in check from a knight ***/
    mask = init_pos.player[opponent_turn].Knights;
    while(mask != 0){
      if(knight_attacks[__builtin_ctzll(mask)] & square){return(true);}
      mask &= (mask - 1);
    }
    /*** Verify that it is not in check from a pawn ***/
    mask = init_pos.player[opponent_turn].Pawns;
    while(mask != 0){
      if(pawn_capture_attacks[opponent_turn][__builtin_ctzll(mask)] & square){return(true);}
      mask &= (mask - 1);
    }
    /*** Verify that it is not in check from a bishop, rook or queen ***/
    unsigned int pos;
    unsigned long long blockers = (init_pos.player[opponent_turn].Pawns |
				   init_pos.player[opponent_turn].Bishops |
				   init_pos.player[opponent_turn].Knights |
				   init_pos.player[opponent_turn].Rooks |
				   init_pos.player[opponent_turn].Queens |
				   init_pos.player[opponent_turn].King |
				   init_pos.player[turn].Pawns |
				   init_pos.player[turn].Bishops |
				   init_pos.player[turn].Knights |
				   init_pos.player[turn].Rooks |
				   init_pos.player[turn].Queens
				   //| init_pos.player[turn].King | //No need to include own king in the blockers
				   ) ;
    /*** check from Bishops ***/
    mask = init_pos.player[opponent_turn].Bishops;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      if(bishop_attacks[pos][((blockers & bishop_masks[pos]) * bishop_magics[pos]) >> bishop_shifts[pos]] & square){return(true);}
      mask &= (mask - 1);
    }
    /*** check from Rooks ***/
    mask = init_pos.player[opponent_turn].Rooks;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      if(rook_attacks[pos][((blockers & rook_masks[pos]) * rook_magics[pos]) >> rook_shifts[pos]] & square){return(true);}
      mask &= (mask - 1);
    }
    /*** check from Queens ***/
    mask = init_pos.player[opponent_turn].Queens;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      if((bishop_attacks[pos][((blockers & bishop_masks[pos]) * bishop_magics[pos]) >> bishop_shifts[pos]] |
	  rook_attacks[pos][  ((blockers & rook_masks[pos])   * rook_magics[pos]  ) >> rook_shifts[pos]] ) & square){return(true);}
      mask &= (mask - 1);
    }
    /*** Not in check ***/
    return(false);
  }

  bool verifyIfDrawn(const Position& init_pos,
		     unsigned short int turn){
    
    unsigned short int opponent_turn = (turn + 1) % 2;
    if(init_pos.player[turn].Queens |
       init_pos.player[turn].Rooks  |
       init_pos.player[turn].Pawns  |
       init_pos.player[opponent_turn].Queens |
       init_pos.player[opponent_turn].Rooks  |
       init_pos.player[opponent_turn].Pawns){
      return(false);
    }
    if((2 <= __builtin_popcountll(init_pos.player[turn].Bishops)) ||
       (2 <= __builtin_popcountll(init_pos.player[opponent_turn].Bishops))){
      //This may fail if a player has under-promoted and has two bishops on the same color squares.
      return(false);
    }
    if(((1 <= __builtin_popcountll(init_pos.player[turn].Bishops)) &&
	(1 <= __builtin_popcountll(init_pos.player[turn].Knights))) ||
       ((1 <= __builtin_popcountll(init_pos.player[opponent_turn].Bishops)) &&
	(1 <= __builtin_popcountll(init_pos.player[opponent_turn].Knights)))){
      return(false);
    }
    return(true);
  }
  
  unsigned long long allAttacks(const Position& init_pos, unsigned short int turn){
    unsigned short int opponent_turn = (turn + 1) % 2;
    unsigned long long all_attacks = 0ULL;
    unsigned int pos = 0;

    unsigned long long mask = 0ULL;
    
    const unsigned long long own_blockers = (init_pos.player[turn].Pawns   |
					     init_pos.player[turn].Bishops |
					     init_pos.player[turn].Knights |
					     init_pos.player[turn].Rooks   |
					     init_pos.player[turn].Queens  |
					     init_pos.player[turn].King);
      
    const unsigned long long opp_blockers = (init_pos.player[opponent_turn].Pawns   |
					     init_pos.player[opponent_turn].Bishops |
					     init_pos.player[opponent_turn].Knights |
					     init_pos.player[opponent_turn].Rooks   |
					     init_pos.player[opponent_turn].Queens  |
					     init_pos.player[opponent_turn].King) ;
    
    unsigned long long all_blockers =  own_blockers | opp_blockers;
    unsigned long long opp_king     = init_pos.player[opponent_turn].King;
    
    //King attacks
    all_attacks |= (king_attacks[__builtin_ctzll(init_pos.player[turn].King)] & (~(own_blockers | opp_king)));
    //Knight attacks
    mask = init_pos.player[turn].Knights;
    while(mask != 0){
      all_attacks |= (knight_attacks[__builtin_ctzll(mask)] & (~(own_blockers | opp_king)));
      mask &= (mask - 1);
    }
    //Pawn attacks
    mask = init_pos.player[turn].Pawns;
    while(mask != 0){
      all_attacks |= (pawn_capture_attacks[turn][__builtin_ctzll(mask)] & (~(own_blockers | opp_king)));
      mask &= (mask - 1);
    }
    //Bishop attacks
    mask = init_pos.player[turn].Bishops;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      all_blockers = (own_blockers | opp_blockers) & (~(1ULL << pos));
      all_blockers &= bishop_masks[pos];
      all_attacks |= (bishop_attacks[pos][((all_blockers & bishop_masks[pos]) * bishop_magics[pos]) >> bishop_shifts[pos]] &
		      (~(own_blockers | opp_king)));
      mask &= (mask - 1);
    }
    //Rook attacks
    mask = init_pos.player[turn].Rooks;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      all_blockers = (own_blockers | opp_blockers) & (~(1ULL << pos));
      all_blockers &= rook_masks[pos];
      all_attacks |= (rook_attacks[pos][((all_blockers & rook_masks[pos]) * rook_magics[pos]) >> rook_shifts[pos]] &
		      (~(own_blockers | opp_king)));
      mask &= (mask - 1);
    }
    //Queen attacks
    mask = init_pos.player[turn].Queens;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      all_blockers = (own_blockers | opp_blockers) & (~(1ULL << pos));
      all_attacks |= ((bishop_attacks[pos][((all_blockers & bishop_masks[pos]) * bishop_magics[pos]) >> bishop_shifts[pos]] |
		       rook_attacks[pos][  ((all_blockers & rook_masks[pos])   * rook_magics[pos]  ) >> rook_shifts[pos]] ) &
		      (~(own_blockers | opp_king)));
      mask &= (mask - 1);
    }
    return(all_attacks);
  }
  
  /* Use this to check if own king is attacked 
     by any of the opponent pieces, using only 
     the opponent pieces as blockers. 
     If it is not, the king cannot be in check 
     regardless on where our own pieces are placed.*/
  unsigned long long unblockedOpponentAttacks(const Position& init_pos, unsigned short int turn){
    unsigned short int opponent_turn = (turn + 1) % 2;
    unsigned long long mask = 0ULL;
    unsigned int pos = 0;
    unsigned long long opp_blockers = (init_pos.player[opponent_turn].Pawns |
				       init_pos.player[opponent_turn].Bishops |
				       init_pos.player[opponent_turn].Knights |
				       init_pos.player[opponent_turn].Rooks |
				       init_pos.player[opponent_turn].Queens |
				       init_pos.player[opponent_turn].King);
    
    unsigned long long unblocked_opponent_attacks = king_attacks[__builtin_ctzll(init_pos.player[opponent_turn].King)];
 
    for(mask = init_pos.player[opponent_turn].Pawns; mask != 0; mask &= (mask - 1)){
      unblocked_opponent_attacks |= pawn_capture_attacks[opponent_turn][__builtin_ctzll(mask)];
    }
    for(mask = init_pos.player[opponent_turn].Knights; mask != 0; mask &= (mask - 1)){
      unblocked_opponent_attacks |= knight_attacks[__builtin_ctzll(mask)];
    }
    for(mask = init_pos.player[opponent_turn].Bishops; mask != 0; mask &= (mask - 1)){
      pos = __builtin_ctzll(mask);
      unblocked_opponent_attacks |= bishop_attacks[pos][((opp_blockers & bishop_masks[pos]) * bishop_magics[pos]) >> bishop_shifts[pos]];
    }
    for(mask = init_pos.player[opponent_turn].Rooks; mask != 0; mask &= (mask - 1)){
      pos = __builtin_ctzll(mask);
      unblocked_opponent_attacks |= rook_attacks[pos][((opp_blockers & rook_masks[pos]) * rook_magics[pos]) >> rook_shifts[pos]];
    }
    for(mask = init_pos.player[opponent_turn].Queens; mask != 0; mask &= (mask - 1)){
      pos = __builtin_ctzll(mask);
      unblocked_opponent_attacks |= bishop_attacks[pos][((opp_blockers & bishop_masks[pos]) * bishop_magics[pos]) >> bishop_shifts[pos]];
      unblocked_opponent_attacks |= rook_attacks[pos][  ((opp_blockers & rook_masks[pos])   * rook_magics[pos]  ) >> rook_shifts[pos]];
    }
    return(unblocked_opponent_attacks);
  }
  
  long int evaluateOneSide(const Position& init_pos,
			   unsigned short int turn,
			   unsigned long long unblocked_opponent_attacks,
			   unsigned long long all_opponent_attacks,
			   unsigned long long allOpponentBlockers){   
    long int score = 0;    
    unsigned int pos;
    unsigned short int opp_turn = (1+turn)%2;
    unsigned short int file;
    
    unsigned long long allOwnBlockers = (init_pos.player[turn].Pawns | 
					 init_pos.player[turn].Bishops | 
					 init_pos.player[turn].Knights | 
					 init_pos.player[turn].Rooks | 
					 init_pos.player[turn].King | 
					 init_pos.player[turn].Queens);
    
    unsigned long long opponentKing = init_pos.player[opp_turn].King;
    
    //Sliders only:
    //Rook: 
    unsigned long long mask, new_mask;
    unsigned long long blockers, rook_moves, bishop_moves, queen_moves, knight_moves, king_moves, pawn_moves, pawn_captures;
    unsigned long long all_own_attacks = 0ULL;
        
    mask = init_pos.player[turn].Rooks;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      blockers = (allOpponentBlockers | opponentKing | allOwnBlockers) & (~(1ULL << pos));
      blockers &= rook_masks[pos];
      //Do not remove own blockers as we want to see which own pieces are defended by the rook
      rook_moves = rook_attacks[pos][(blockers * rook_magics[pos]) >> rook_shifts[pos]];
      //Remove only opponent king, which cannot be captured
      rook_moves &= ~opponentKing;      
      score += rook_values[turn][pos];

      //Mobility score, MOBILITY_BONUS value for each square.
      score += __builtin_popcountll(rook_moves & (~allOwnBlockers)) * MOBILITY_BONUS;
      
      if((1ULL << pos) & all_opponent_attacks){//Rook attacked
	score -= ATTACKED_ROOK_PENALTY;
      }
      
      //Check for connected rooks
      if(rook_moves & mask){ //Using mask in place of init_pos.player[turn].Rooks avoids double-counting
	score += CONNECTED_PIECES_BONUS;
      }

      /* Add a bonus for the pieces the rook defends */
      new_mask = rook_moves & init_pos.player[turn].Pawns;
      while(new_mask){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_PAWN_BONUS;
	new_mask &= (new_mask - 1);
      }

      new_mask = rook_moves & init_pos.player[turn].Bishops;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_BISHOP_BONUS;
	new_mask &= (new_mask - 1);
      }

      new_mask = rook_moves & init_pos.player[turn].Knights;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_KNIGHT_BONUS;
	new_mask &= (new_mask - 1);
      }

      //Defended rooks already taken into account as connected rooks
      new_mask = rook_moves & init_pos.player[turn].Queens;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_QUEEN_BONUS;
	new_mask &= (new_mask - 1);
      }
      all_own_attacks |= rook_moves;
      mask &= (mask - 1);
    }
    
    mask = init_pos.player[turn].Bishops;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      blockers = (allOpponentBlockers | opponentKing | allOwnBlockers) & (~(1ULL << pos)); 
      blockers &= bishop_masks[pos];
      //Do not remove own blockers as we want to see which own pieces are defended by the bishop.
      bishop_moves = bishop_attacks[pos][(blockers * bishop_magics[pos]) >> bishop_shifts[pos]];
      //Remove only the opponent king, which cannot be captured.
      bishop_moves &= ~opponentKing;
      score += bishop_values[turn][pos];
      score += __builtin_popcountll(bishop_moves & (~allOwnBlockers)) * MOBILITY_BONUS;
      
      if((1ULL << pos) & all_opponent_attacks){//Bishop attacked
	score -= ATTACKED_BISHOP_PENALTY;
      }

      /* Add a bonus for the pieces the bishop defends */
      new_mask = bishop_moves & init_pos.player[turn].Pawns;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_PAWN_BONUS;
	new_mask &= (new_mask - 1);
      }

      //Bishops cannot defend each other unless two are on the same color after pawn promotion. Very unlikely, will ingore the possibility.

      new_mask = bishop_moves & init_pos.player[turn].Knights;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_KNIGHT_BONUS;
	new_mask &= (new_mask - 1);
      }

      new_mask = bishop_moves & init_pos.player[turn].Rooks;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_ROOK_BONUS;
	new_mask &= (new_mask - 1);
      }
      
      new_mask = bishop_moves & init_pos.player[turn].Queens;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_QUEEN_BONUS;
	new_mask &= (new_mask - 1);
      }
      all_own_attacks |= bishop_moves;
      mask &= (mask - 1);
    }

    mask = init_pos.player[turn].Queens;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      blockers = (allOpponentBlockers | opponentKing | allOwnBlockers) & (~(1ULL << pos));
      //Do not remove own blockers, as we want to see which pieces are defended by the queen. 
      queen_moves = rook_attacks[pos][((blockers & rook_masks[pos]) * rook_magics[pos]) >> rook_shifts[pos]];
      queen_moves |= bishop_attacks[pos][((blockers & bishop_masks[pos]) * bishop_magics[pos]) >> bishop_shifts[pos]];
      //Remove only the opponent king, which cannot be captured.
      queen_moves &= ~opponentKing;

      score += queen_values[turn][pos];
      score += __builtin_popcountll(queen_moves & (~allOwnBlockers)) * MOBILITY_BONUS;

      if((1ULL << pos) & all_opponent_attacks){//Queen en prise, reduce value by half
	score -= QUEEN_EN_PRISE_PENALTY;
      }
 
      //If multiple queens on the board, check if they are connected
      if(queen_moves & mask){ //Using mask in place of init_pos.player[turn].Knights avoids double-counting
	score += CONNECTED_PIECES_BONUS;
      }

      /* Add a bonus for the pieces the queen defends */
      new_mask = queen_moves & init_pos.player[turn].Pawns;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_PAWN_BONUS;
	new_mask &= (new_mask - 1);
      }

      new_mask = queen_moves & init_pos.player[turn].Bishops;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_BISHOP_BONUS;
	new_mask &= (new_mask - 1);
      }
      
      new_mask = queen_moves & init_pos.player[turn].Knights;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_KNIGHT_BONUS;
	new_mask &= (new_mask - 1);
      }

      new_mask = queen_moves & init_pos.player[turn].Rooks;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_ROOK_BONUS;
	new_mask &= (new_mask - 1);
      }
      all_own_attacks |= queen_moves;
      mask &= (mask - 1);
    }

    /********* Non-sliders *********/
    mask = init_pos.player[turn].Knights;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      blockers = (allOpponentBlockers | opponentKing | allOwnBlockers) & (~(1ULL << pos)); 
      blockers &= knight_attacks[pos];
      knight_moves = knight_attacks[pos];
      //Do not remove own blockers in order to find which own pieces are defended by the knight.
      //Remove only the opponent king, which cannot be captured. 
      knight_moves &= ~opponentKing;      
      score += knight_values[turn][pos];
      score += __builtin_popcountll(knight_moves & (~allOwnBlockers)) * MOBILITY_BONUS;

      if((1ULL << pos) & all_opponent_attacks){//Knight attacked
	score -= ATTACKED_KNIGHT_PENALTY;
      }

      //Check for connected knights
      if(knight_moves & mask){ //Using mask in place of init_pos.player[turn].Knights avoids double-counting
	score += CONNECTED_PIECES_BONUS;
      }

      //Add a bonus for the pieces defended by the knight
      new_mask = knight_moves & init_pos.player[turn].Pawns;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_PAWN_BONUS;
	new_mask &= (new_mask - 1);
      }

      new_mask = knight_moves & init_pos.player[turn].Bishops;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_BISHOP_BONUS;
	new_mask &= (new_mask - 1);
      }
      
      new_mask = knight_moves & init_pos.player[turn].Rooks;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_ROOK_BONUS;
	new_mask &= (new_mask - 1);
      }
      
      new_mask = knight_moves & init_pos.player[turn].Queens;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_QUEEN_BONUS;
	new_mask &= (new_mask - 1);
      }

      //Check for knight forks. We consider checks, major pieces and bishops (opponent knights can capture if attacked)
      new_mask = (init_pos.player[opp_turn].Bishops | init_pos.player[opp_turn].Rooks |
		  init_pos.player[opp_turn].Queens | init_pos.player[opp_turn].King);
      if(__builtin_popcountll(knight_moves & new_mask)){
	score += KNIGHT_FORK_BONUS;
      }
      all_own_attacks |= knight_moves;
      mask &= (mask - 1);
    }
    
    mask = init_pos.player[turn].King;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      blockers = (allOpponentBlockers | opponentKing | allOwnBlockers) & (~(1ULL << pos));
      blockers &= king_attacks[pos];
      king_moves = king_attacks[pos];
      //Do not remove own blockers in order to find which own pieces are defended by the king.
      //Remove only the opponent king, which cannot be captured. 
      king_moves &= ~opponentKing;
      score += king_values[turn][pos];
      score += __builtin_popcountll(king_moves & (~allOwnBlockers)) * MOBILITY_BONUS;

      if((1ULL << pos) & unblocked_opponent_attacks){//King in check, but blocked by own piece. Also a penalty for pinned piece.
	score -= KING_XRAY_CHECK_PENALTY;
      }

      //Add a bonus for the pieces defended by the king
      new_mask = king_moves & init_pos.player[turn].Pawns;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_PAWN_BONUS;
	new_mask &= (new_mask - 1);
      }

      new_mask = king_moves & init_pos.player[turn].Bishops;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_BISHOP_BONUS;
	new_mask &= (new_mask - 1);
      }

      new_mask = king_moves & init_pos.player[turn].Knights;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_KNIGHT_BONUS;
	new_mask &= (new_mask - 1);
      }
      new_mask = king_moves & init_pos.player[turn].Rooks;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_ROOK_BONUS;
	new_mask &= (new_mask - 1);
      }
      
      new_mask = king_moves & init_pos.player[turn].Queens;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_QUEEN_BONUS;
	new_mask &= (new_mask - 1);
      }
      
      all_own_attacks |= king_moves;
      mask &= (mask - 1);
    }
    mask = init_pos.player[turn].Pawns;
    if(mask & (mask >> 8)){ //Penalty for doubled pawns
      score -= __builtin_popcountll(mask & (mask >> 8)) * DOUBLED_PAWNS_PENALTY;
    }
    
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      blockers = ((allOpponentBlockers | 
		   opponentKing | 
		   allOwnBlockers) & 
		  (~(1ULL << pos))); //Exclude the piece itself from the blockers.
      
      blockers   &= pawn_attacks[turn][pos]; //Keep only blockers that overlap with the actual moves
      pawn_moves  = blockers & pawn_attacks[turn][pos];
      pawn_moves  = (pawn_moves ^ pawn_attacks[turn][pos]) & ((turn == 0) ? ~(blockers >> 8) : ~(blockers << 8));
      score += pawn_values[turn][pos];
      score += __builtin_popcountll(pawn_moves | (pawn_capture_attacks[turn][pos] & allOpponentBlockers)) * MOBILITY_BONUS;

      if((1ULL << pos) & all_opponent_attacks){//Pawn attacked
	score -= ATTACKED_PAWN_PENALTY;
      }
      
      if(init_pos.player[turn].Pawns & pawn_capture_attacks[turn][pos]){ /*Check if defended by other pawns. 
								 They are located on the squares 
								 corresponding to the opponent's pawn capture moves. */
	score += __builtin_popcountll(init_pos.player[turn].Pawns & pawn_capture_attacks[turn][pos]) * DEFENDED_PAWN_BONUS;
      }

      /*Check for doubled and isolated pawns*/
      file = pos % 8;
      if(file == 0){
	if((init_pos.player[turn].Pawns & (A_FILE << 1)) == 0ULL){
	  score -= ISOLATED_PAWN_PENALTY;
	}
      }
      else{
	if(file == 7){
	  if((init_pos.player[turn].Pawns & (A_FILE << 6)) == 0ULL){
	    score -= ISOLATED_PAWN_PENALTY;
	  }
	}
	else{
	  if((init_pos.player[turn].Pawns & ((A_FILE << (file - 1)) | (A_FILE << (file + 1)))) == 0ULL){
	    score -= ISOLATED_PAWN_PENALTY;
	  }
	}
      }
      
      pawn_captures = pawn_capture_attacks[turn][pos];
      //Add a bonus for the pieces defended by this pawn
      new_mask = pawn_captures & init_pos.player[turn].Bishops;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_BISHOP_BONUS;
	new_mask &= (new_mask - 1);
      }

      new_mask = pawn_captures & init_pos.player[turn].Knights;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_KNIGHT_BONUS;
	new_mask &= (new_mask - 1);
      }
      
      new_mask = pawn_captures & init_pos.player[turn].Rooks;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_ROOK_BONUS;
	new_mask &= (new_mask - 1);
      }
      
      new_mask = pawn_captures & init_pos.player[turn].Queens;
      while(new_mask != 0){
	pos = __builtin_ctzll(new_mask);
	score += DEFENDED_QUEEN_BONUS;
	new_mask &= (new_mask - 1);
      }

      all_own_attacks |= pawn_moves;
      mask &= (mask - 1);
    }

    /************************ Check for hanging pieces ************************/

    mask = init_pos.player[turn].Pawns & (~all_own_attacks) & all_opponent_attacks;
    if(mask != 0ULL){
      while(mask != 0){
	pos = __builtin_ctzll(mask);
	score -= pawn_values[turn][pos];
	mask &= (mask - 1);
      }
    }

    mask = init_pos.player[turn].Bishops & (~all_own_attacks) & all_opponent_attacks;
    if(mask != 0ULL){
      while(mask != 0){
	pos = __builtin_ctzll(mask);
	score -= bishop_values[turn][pos];
	mask &= (mask - 1);
      }
    }

    mask = init_pos.player[turn].Knights & (~all_own_attacks) & all_opponent_attacks;
    if(mask != 0ULL){
      while(mask != 0){
	pos = __builtin_ctzll(mask);
	score -= knight_values[turn][pos];
	mask &= (mask - 1);
      }
    }

    mask = init_pos.player[turn].Rooks & (~all_own_attacks) & all_opponent_attacks;
    if(mask != 0ULL){
      while(mask != 0){
	pos = __builtin_ctzll(mask);
	score -= rook_values[turn][pos];
	mask &= (mask - 1);
      }
    }
    
    mask = init_pos.player[turn].Queens & (~all_own_attacks) & all_opponent_attacks;
    if(mask != 0ULL){
      while(mask != 0){
	pos = __builtin_ctzll(mask);
	score -= queen_values[turn][pos];
	mask &= (mask - 1);
      }
    }
        
    return(score);
  }

  long int evaluate(const Position& init_pos, unsigned short int turn){
    
    unsigned short int opponent_turn = (1 + turn)%2;
    
    unsigned long long own_blockers = (init_pos.player[turn].Pawns |
				       init_pos.player[turn].Bishops |
				       init_pos.player[turn].Knights |
				       init_pos.player[turn].Rooks |
				       init_pos.player[turn].Queens);

    unsigned long long opp_blockers = (init_pos.player[opponent_turn].Pawns |
				       init_pos.player[opponent_turn].Bishops |
				       init_pos.player[opponent_turn].Knights |
				       init_pos.player[opponent_turn].Rooks |
				       init_pos.player[opponent_turn].Queens);
    
    long int own_score = evaluateOneSide(init_pos, turn,
					 unblockedOpponentAttacks(init_pos, turn),
					 allAttacks(init_pos, opponent_turn),
					 opp_blockers);
    
    long int opp_score = evaluateOneSide(init_pos, (1 + turn)%2,
					 unblockedOpponentAttacks(init_pos, (1 + turn)%2),
					 allAttacks(init_pos, (1 + opponent_turn)%2),
					 own_blockers);

    /*************************************************************************
     * Use the simple eval function to see how the play changes when only the 
     * value of the pieces is taken into account and not their interaction.
     *************************************************************************/
    /* 
       long int own_score = evaluateOneSideSimple(init_pos, turn);
       long int opp_score = evaluateOneSideSimple(init_pos, (1 + turn)%2);
    */
    return(own_score - opp_score);
  }

  long int evaluateOneSideSimple(const Position& init_pos,
				 unsigned short int turn){   
    long int score = 0;    
    unsigned int pos;
    unsigned long long mask;
        
    mask = init_pos.player[turn].Rooks;
    while(mask != 0){
      pos = __builtin_ctzll(mask);      
      score += rook_values[turn][pos];
      mask &= (mask - 1);
    }
    
    mask = init_pos.player[turn].Bishops;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      score += bishop_values[turn][pos];
      mask &= (mask - 1);
    }

    mask = init_pos.player[turn].Queens;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      score += queen_values[turn][pos];
      mask &= (mask - 1);
    }

    /********* Non-sliders *********/
    mask = init_pos.player[turn].Knights;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      score += knight_values[turn][pos];
      mask &= (mask - 1);
    }
    
    mask = init_pos.player[turn].King;
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      score += king_values[turn][pos];
      mask &= (mask - 1);
    }
    
    mask = init_pos.player[turn].Pawns;    
    while(mask != 0){
      pos = __builtin_ctzll(mask);
      score += pawn_values[turn][pos];
      mask &= (mask - 1);
    }       
    return(score);
  }
}
