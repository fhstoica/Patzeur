This is a simple chess engint that makes use of bitboards for move generation.
To run  it simply use "make" and then run with 

./patzeur.srl

The engine plays black by default so it will wait for the player with the 
white pieces to make the first move. Moves are input in the format 
start square-final square. For example to open e4, we will write:

e2-e4

To start from an arbitrary position you'll first need the position written in 
a file following the format shown in the file: 

sample_position.dat

and then run with: 

./patzeur.srl sample_position.dat

To undo a move simply type "back". To castle type "0-0" or "0-0-0". To end the 
game simply type "x". When you exit the game, the engine will write out the 
moves played in the game in the file "moves.log". 

To see the moves in a more friendly format, use the Python script provided:

./parse_move_log.py moves.log

The output is called "output_moves.log" and it is more human-readable. 

THe engine makes use of a transposition table, quiescense search (captures only 
past a certain depth) but uses simple alpha-beta minimax for the search, 
not the more advanced MTD-f algorightm. By default will run 4 threads for the 
search. You can change the number of threads and the size of the hash table 
by modifying the file "staticDefinitions.h".

Happy hacking !

PS: For the magic constants used in the generation of the moves I have to 
thank github user hxhl95 for the example code he provided: 
https://github.com/hxhl95/chess-ai

