#ifndef STRUCTS_H
#define STRUCTS_H

#include <stdlib.h>
#include <math.h>
#include <inttypes.h>

typedef struct Board{
  unsigned long long Pawns;
  unsigned long long Bishops;
  unsigned long long Knights;
  unsigned long long Rooks;
  unsigned long long Queens;
  unsigned long long King;
  unsigned short int castleL;
  unsigned short int castleS;
} Board;

typedef struct Position{
  Board player[2]; //black 0, white 1
  unsigned long long en_passant;
  unsigned long long Zobrist_hash;
} Position;

#endif
