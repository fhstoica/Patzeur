#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <map>
#include <set>
#include <list>
#include <vector>
#include <utility>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <limits>
#include <ios>
#include <iomanip>
#include <chrono> 

#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include "structs.h"
#include "utils.h"
#include "IO.h"
#include "hashing.h"
#include "movesGen.h"
#include "checksAndEvals.h"
#include "treeSearch.h"
#include "staticDefinitions.h"

std::string toStringTurn(unsigned short int turn){
  if(turn == 1){
    return("white");
  }
  return("black");
}

void testSimpleSearch(std::string file_name = ""){
  unsigned long long counteur = 0ULL;
  unsigned long long transp_table_counteur = 0ULL;
  unsigned int max_depth      = 0;
  Position init_pos, final_pos;
  
  Search::setInitialPosition(init_pos);
  Search::setInitialPosition(final_pos);
  if(file_name.size() != 0){
    IO::readPosition(init_pos, file_name);  
  }
  unsigned short int turn                   = 1;//white starts
  unsigned short int opponent_turn          = (1 + turn)%2;
  int quiescence_depth                      = 7; //Past thid depth we consider only captures
  unsigned long long total_positions_budget = 10000000ULL;//Total move budget.
  std::list<Position> played_moves;
  played_moves.push_back(init_pos);

  Hashing::initHashTable();
  
  while(true){
    IO::printPositionColor(init_pos);
    Position old_pos = init_pos;
    if(!IO::readMoveFromKeyboard(played_moves, init_pos, turn)){
      continue;
    }

    auto t1 = std::chrono::high_resolution_clock::now();
    std::cout << IO::diffPositions(old_pos,  init_pos) << std::endl;

    init_pos.Zobrist_hash = Hashing::computeHash(init_pos);
    IO::printPositionColor(init_pos);
    Search::topLevelSearchBestPosMT(init_pos,
				    final_pos,
				    played_moves,
				    opponent_turn,
				    opponent_turn,
				    total_positions_budget,
				    max_depth,
				    quiescence_depth,
				    false,
				    counteur,
				    transp_table_counteur,
				    played_moves.size());

    auto t2 = std::chrono::high_resolution_clock::now();
    std::cout << "Turn : "              << toStringTurn(opponent_turn) 
	      << " PosPlayed : "        << played_moves.size() 
	      << " PosAnalysed : "      << counteur 
	      << " max_depth : "        << max_depth
	      << " trasp_table_hits : " << transp_table_counteur
	      << " time : "             <<  std::chrono::duration_cast<std::chrono::seconds>(t2 - t1).count()<< std::endl;
    played_moves.push_back(final_pos);

    Hashing::combineHashTables();
    Hashing::purgeHashTable(played_moves.size());

    if(Utils::checkThreefoldRepetition(played_moves)){
      break;
    } 
    std::cout << IO::diffPositions(init_pos, final_pos) << std::endl;
    init_pos = final_pos;
  }
}

int main(int argc, char* argv[]){
  if(argc == 2){
    std::string file_name(argv[1]);
    testSimpleSearch(file_name);
  }
  else{
    testSimpleSearch();
  }
  return(0);
}
