#include <iostream>
#include "constants.h"
#include "keyboardInput.h"


namespace KeyboardInput{
  void generateCastleShortKeyboardInput(Position& init_pos, unsigned short int turn){
    init_pos.player[turn].Rooks &= (~rook_init_pos_castle_short[turn]); //Remvoe rook from old position
    init_pos.player[turn].Rooks |=  rook_final_pos_castle_short[turn];  //Add rook in final position
    init_pos.player[turn].King = (init_pos.player[turn].King << 2); //Move the king 2 squares to the right
    init_pos.en_passant = 0ULL;
  }

  void generateCastleLongKeyboardInput(Position& init_pos, unsigned short int turn){
    init_pos.player[turn].Rooks &= (~rook_init_pos_castle_long[turn]); //Remvoe rook from old position
    init_pos.player[turn].Rooks |=  rook_final_pos_castle_long[turn];  //Add rook in final position
    init_pos.player[turn].King = (init_pos.player[turn].King >> 2); //Move the king 2 squares to the left
    init_pos.en_passant = 0ULL;
  }

  void removeCapturedPieceKeyboardInput(Position& init_pos, unsigned short int turn, unsigned long long capture_square){
    unsigned short int opponent_turn = (1 + turn)%2;
    if(init_pos.player[opponent_turn].Pawns & capture_square){
      init_pos.player[opponent_turn].Pawns &= (~capture_square);
      return;
    }
    if(init_pos.player[opponent_turn].Knights & capture_square){
      init_pos.player[opponent_turn].Knights &= (~capture_square);
      return;
    }
    if(init_pos.player[opponent_turn].Bishops & capture_square){
      init_pos.player[opponent_turn].Bishops &= (~capture_square);
      return;
    }
    if(init_pos.player[opponent_turn].Rooks & capture_square){
      init_pos.player[opponent_turn].Rooks &= (~capture_square);
      return;
    }
    if(init_pos.player[opponent_turn].Queens & capture_square){
      init_pos.player[opponent_turn].Queens &= (~capture_square);
      return;
    }
    if(init_pos.en_passant & capture_square){
      if(opponent_turn == 1){//White pawn captured en passant
	init_pos.player[opponent_turn].Pawns &= (~(capture_square << 8));
      }
      if(opponent_turn == 0){//Black pawn captured en passant
	init_pos.player[opponent_turn].Pawns &= (~(capture_square >> 8));
      }
      init_pos.en_passant = 0ULL;
      return;
    }
  }

  void promotePieceKeyboardInput(Position& init_pos, unsigned short int turn, unsigned long long promotion_square){
    std::string piece_promotion;
    while(true){
      std::cout << "Which piece to promote ? (b, n, r, q)" << std::endl;
      std::cin >> piece_promotion;
      if(piece_promotion.size() != 1){
	std::cout << "Please use one of the indicated letters to promote" << std::endl;
	continue;
      }
      char piece = piece_promotion[0];
      switch(piece){
      case 'b':
	init_pos.player[turn].Pawns &= (~promotion_square);
	init_pos.player[turn].Bishops |= promotion_square;
	break;
      case 'n':
	init_pos.player[turn].Pawns &= (~promotion_square);
	init_pos.player[turn].Knights |= promotion_square;
	break;
      case 'r':
	init_pos.player[turn].Pawns &= (~promotion_square);
	init_pos.player[turn].Rooks |= promotion_square;
	break;
      case 'q':
	init_pos.player[turn].Pawns &= (~promotion_square);
	init_pos.player[turn].Queens |= promotion_square;
	break;
      default:
	std::cout << "Cannot promote piece : " << piece_promotion << std::endl;
	continue;
	break;
      }
      init_pos.en_passant = 0ULL;
      return;
    }
  }

  void movePieceKeyboardInput(Position& init_pos, unsigned short int turn, unsigned long long start_square, unsigned long long end_square){
    unsigned short int opponent_turn = (1 + turn)%2;
    unsigned long long all_opp_capture_pieces = 0ULL;
    all_opp_capture_pieces = (init_pos.player[opponent_turn].Pawns | 
			      init_pos.player[opponent_turn].Bishops | 
			      init_pos.player[opponent_turn].Knights | 
			      init_pos.player[opponent_turn].Rooks | 
			      init_pos.player[opponent_turn].Queens);
    
    
    if(init_pos.player[turn].Pawns & start_square){//Pawn move
      init_pos.player[turn].Pawns &= (~start_square);
      init_pos.player[turn].Pawns |= end_square;
      
      if(all_opp_capture_pieces & end_square){//Capture took place, remove the captured piece
	removeCapturedPieceKeyboardInput(init_pos, turn, end_square);
      }

      if(promotion_rank[turn] & end_square){
	promotePieceKeyboardInput(init_pos, turn, end_square);
      }
      
      if(turn == 0){//Black to move
	if((start_square >> 16 ) & end_square){
	  init_pos.en_passant = start_square >> 8;
	}
	else{
	  init_pos.en_passant = 0ULL;
	}
      }
      else{//White to move
	if((start_square  << 16 ) & end_square){
	  init_pos.en_passant = start_square << 8;
	}
	else{
	  init_pos.en_passant = 0ULL;
	}
      }
      return;
    }
    
    if(init_pos.player[turn].Knights & start_square){
      init_pos.player[turn].Knights &= (~start_square);
      init_pos.player[turn].Knights |= end_square;
      if(all_opp_capture_pieces & end_square){
	removeCapturedPieceKeyboardInput(init_pos, turn, end_square);
      }
      init_pos.en_passant = 0ULL;
      return;
    }
    if(init_pos.player[turn].Bishops & start_square){
      init_pos.player[turn].Bishops &= (~start_square);
      init_pos.player[turn].Bishops |= end_square;
      if(all_opp_capture_pieces & end_square){
	removeCapturedPieceKeyboardInput(init_pos, turn, end_square);
      }
      init_pos.en_passant = 0ULL;
      return;
    }
    if(init_pos.player[turn].Rooks & start_square){
      init_pos.player[turn].Rooks &= (~start_square);
      init_pos.player[turn].Rooks |= end_square;
      if(all_opp_capture_pieces & end_square){
	removeCapturedPieceKeyboardInput(init_pos, turn, end_square);
      }
      init_pos.en_passant = 0ULL;
      return;
    }
    if(init_pos.player[turn].Queens & start_square){
      init_pos.player[turn].Queens &= (~start_square);
      init_pos.player[turn].Queens |= end_square;
      if(all_opp_capture_pieces & end_square){
	removeCapturedPieceKeyboardInput(init_pos, turn, end_square);
      }
      init_pos.en_passant = 0ULL;
      return;
    }
    if(init_pos.player[turn].King & start_square){
      init_pos.player[turn].King &= (~start_square);
      init_pos.player[turn].King |= end_square;
      if(all_opp_capture_pieces & end_square){
	removeCapturedPieceKeyboardInput(init_pos, turn, end_square);
      }
      init_pos.en_passant = 0ULL;
      return;
    }
    if(init_pos.en_passant & end_square){
      if(opponent_turn == 1){//White pawn captured en passant
	init_pos.player[turn].Pawns &= (~start_square);
	init_pos.player[turn].Pawns |= end_square;
	init_pos.player[opponent_turn].Pawns &= (~(end_square << 8));
      }
      if(opponent_turn == 0){//Black pawn captured en passant
	init_pos.player[turn].Pawns &= (~start_square);
	init_pos.player[turn].Pawns |= end_square;
	init_pos.player[opponent_turn].Pawns &= (~(end_square >> 8));
      }
      init_pos.en_passant = 0ULL;
      return;
    }
  }
}
