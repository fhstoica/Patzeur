#ifndef KEYBOARD_INPUT_H
#define KEYBOARD_INPUT_H
#include <list>
#include "structs.h"
#include "utils.h"

namespace KeyboardInput{
  void generateCastleShortKeyboardInput(Position& init_pos,
					unsigned short int turn);
  void generateCastleLongKeyboardInput(Position& init_pos,
				       unsigned short int turn);
  void removeCapturedPieceKeyboardInput(Position& init_pos, 
					unsigned short int turn, 
					unsigned long long capture_square);
  void promotePieceKeyboardInput(Position& init_pos, 
				 unsigned short int turn, 
				 unsigned long long promotion_square);
  void movePieceKeyboardInput(Position& init_pos, 
			      unsigned short int turn, 
			      unsigned long long start_square, 
			      unsigned long long end_square);
}
#endif
