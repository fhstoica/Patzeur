#ifndef MULTI_THREADED_DISPATCHER_CLASS
#define MULTI_THREADED_DISPATCHER_CLASS

#include <stdlib.h>
#include <stdio.h>
#include <list>
#include <map>
#include <iostream>
#include <pthread.h>

template<typename T> class MultiThreadedDispatcherClass{
public:
  class DataToProcess{
  public:
    typename std::list<T> input_list;
    typename std::list<T> output_list;
  };

private:
  pthread_mutex_t input_queue_mutex;
  pthread_mutex_t output_queue_mutex;
  pthread_attr_t attr;
  DataToProcess* _argument;
  void (*processingFunc)(unsigned int, T*);
  const unsigned int N_CPUS;
  std::map<pthread_t, unsigned int> thread_to_ID_map;

public:
  MultiThreadedDispatcherClass(DataToProcess* _arg,
			       void (*func)(unsigned int, T*),
			       unsigned int NO_CPUS) : _argument(_arg), 
						       processingFunc(func), 
						       N_CPUS(NO_CPUS){
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    pthread_mutex_init(&input_queue_mutex, NULL);
    pthread_mutex_init(&output_queue_mutex, NULL);
  }

  ~MultiThreadedDispatcherClass(){
    pthread_mutex_destroy(&input_queue_mutex);
    pthread_mutex_destroy(&output_queue_mutex);
    pthread_attr_destroy(&attr);
  }

  void* F(void* data_to_process){
    DataToProcess* local_dtp = static_cast<DataToProcess*>(data_to_process);
    bool have_to_process = true;
    while(have_to_process){
      pthread_mutex_lock(&input_queue_mutex);
      /**************************************
       * The thread_to_ID_map is defined in 
       * the scope of the surrounding class
       **************************************/
      pthread_t thread_ID = pthread_self();
      unsigned int thread_ID_int = thread_to_ID_map[thread_ID];
      /********************************
       * Return if the queue is empty.
       ********************************/
      if(0 == local_dtp->input_list.size()){
	pthread_mutex_unlock(&input_queue_mutex);
	have_to_process = false;
      }else{
	/***************************************
	 * Take the last element from the queue
	 * and place it in the temp storage 
	 * with the index equal with the thread 
	 * number. Copy _by value_.
	 ***************************************/
        T local_data = local_dtp->input_list.back();
	local_dtp->input_list.pop_back();
	pthread_mutex_unlock(&input_queue_mutex);
	processingFunc(thread_ID_int, &local_data);
	/******************************************
	 * Take the value from temporary storage  
	 * write it in the output queue _by value_
	 ******************************************/
	pthread_mutex_lock(&output_queue_mutex);
	local_dtp->output_list.push_back(local_data);
	pthread_mutex_unlock(&output_queue_mutex);
      }      
    }
    return(0);
  }

  DataToProcess* getArgument(){
    return _argument;
  }

  static void* G(void* argument){
    MultiThreadedDispatcherClass* _this = static_cast<MultiThreadedDispatcherClass*>(argument);
    _this->F(_this->getArgument());
    return(0);
  }

  void multiThreadedDispatcher(void){
    pthread_t p_thread[N_CPUS];
    for(unsigned int thread_no = 0; thread_no < N_CPUS; ++thread_no){
      int rc = pthread_create(&(p_thread)[thread_no], &attr, &MultiThreadedDispatcherClass::G, static_cast<void*>(this));
      std::pair<pthread_t, unsigned int> p_i(p_thread[thread_no], thread_no);
      thread_to_ID_map.insert(p_i);
      if(0 != rc){
	std::cerr << "Could not create thread" << std::endl;
	exit(-1);
      }
    }
    for(unsigned int i = 0; i < N_CPUS; ++i){
      pthread_join(p_thread[i], NULL);
    } 
  }
};

#endif
