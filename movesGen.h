#ifndef MOVES_GEN_H
#define MOVES_GEN_H
#include <list>
#include "structs.h"
#include "utils.h"

namespace Moves{
  void generateAllMoves(const Position& init_pos, 
			std::list<Position>& legal_moves, 
			unsigned short int turn, 
			bool captures_only);
}
#endif
