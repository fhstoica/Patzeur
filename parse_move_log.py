#!/usr/bin/python

import os, sys, re

class MoveLogParser():

    def __init__(self, log_file):
        self.log_file  = log_file
        self.raw_moves = []
        self.out_moves = []
        self.patt      = re.compile("^[1-7]")
        self.raw_data  = None
        self.file_map  = ["A", "B", "C", "D", "E", "F", "G", "H"]


    def readFile(self):
        self.raw_data = [s.strip() for s in open(self.log_file, "r").readlines() if len(s.strip()) > 0]
    
    def process(self):
        curr_board = []
        for line in self.raw_data:
            if len(line.strip()) == 0: #empty line
                continue
            else:
                self.raw_moves.append(line.strip())
                
        for i in range(len(self.raw_moves) - 1):
            curr_line = self.raw_moves[i+1]
            prev_line = self.raw_moves[i  ]

            diff_idx = []
            for j in range(len(curr_line)):
                if curr_line[j] != prev_line[j]:
                    diff_idx.append(j)
         
            start_idx = None
            end_idx   = None

            if len(diff_idx) == 2:
                if prev_line[diff_idx[0]] != "." and prev_line[diff_idx[1]] != ".": # the move was a capture
                    if curr_line[diff_idx[0]] != "." : # piece captured at diff_idx[0]
                        start_idx = diff_idx[1]
                        end_idx   = diff_idx[0]
                    elif curr_line[diff_idx[1]] != ".":
                        start_idx = diff_idx[0]
                        end_idx   = diff_idx[1]
                    else:
                        raise Exception("cannot find start index")
                elif prev_line[diff_idx[0]] != "." and prev_line[diff_idx[1]] == ".": # move from  diff_idx[0] --> diff_idx[1]
                    start_idx = diff_idx[0]
                    end_idx   = diff_idx[1]
                elif prev_line[diff_idx[0]] == "." and prev_line[diff_idx[1]] != ".": # move from  diff_idx[1] --> diff_idx[0]
                    start_idx = diff_idx[1]
                    end_idx   = diff_idx[0]
                else:
                    raise Exception("Cannot find move")

                start_rank = start_idx // 8
                start_file = start_idx %  8
                
                end_rank   =   end_idx // 8
                end_file   =   end_idx %  8

                piece = curr_line[end_idx]
  
                self.out_moves.append("%s%s --> %s%s %s%s%s" % (self.file_map[start_file], start_rank + 1, 
                                                                self.file_map[end_file], end_rank + 1, 
                                                                piece, self.file_map[end_file], end_rank + 1))
                        
            elif len(diff_idx) == 4:
                white_king_square_00  = 6
                white_king_square_000 = 2
                black_king_square_00  = 62
                black_king_square_000 = 58
                if curr_line[white_king_square_00] == "K" and prev_line[white_king_square_00] == ".":
                    self.out_moves.append("White 0-0")
                elif curr_line[white_king_square_000] == "K" and prev_line[white_king_square_000] == ".":
                    self.out_moves.append("White 0-0-0")
                elif curr_line[black_king_square_00] == "k" or prev_line[black_king_square_00] == ".":
                    self.out_moves.append("Black 0-0")
                elif curr_line[black_king_square_000] == "k" or prev_line[black_king_square_000] == ".":
                    self.out_moves.append("Black 0-0-0")
                else:
                    raise Exception("Bad castling")

            elif len(diff_idx) == 3:
                # En passant capture
                pawn_set = []
                for i in diff_idx:
                    pawn_set.append(prev_line[i])
                    pawn_set.append(curr_line[i])
                
                pawn_set = [s for s in pawn_set if s != "."]    
                capturing_pawn = None
                for s in pawn_set:
                    if pawn_set.count(s) > 1:
                        capturing_pawn = s
                        break

                start_idx = None
                end_idx   = None
                for i in diff_idx:
                    if prev_line[i] == capturing_pawn:
                        start_idx = i
                        break
                        
                for i in diff_idx:
                    if curr_line[i] == capturing_pawn:
                        end_idx = i
                        break

                start_rank = start_idx // 8
                start_file = start_idx %  8
                
                end_rank   =   end_idx // 8
                end_file   =   end_idx %  8
                
                piece = curr_line[end_idx]

                self.out_moves.append("%s%s --> %s%s %s%s%s En passant" % (self.file_map[start_file], start_rank + 1, 
                                                                           self.file_map[end_file], end_rank + 1, 
                                                                           piece, self.file_map[end_file], end_rank + 1))


            elif len(diff_idx) == 0:
                pass
            else:
                raise Exception("Uknown move \n" + prev_line + "\n" + curr_line + " \ndiff_idx = " +str(diff_idx) +"\n")

    def writeData(self):
        fout = open("output_" + self.log_file, "w")
        fout.write("\n".join(self.out_moves))
        fout.close()
    
    def execute(self):
        self.readFile()
        self.process()
        self.writeData()

def main():
    file_in = sys.argv[1]
    X = MoveLogParser(file_in)
    X.execute()

if __name__ == "__main__":
    main()
