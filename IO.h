#ifndef IO_H
#define IO_H

#include <inttypes.h>
#include <string>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include "structs.h"
#include "utils.h"

namespace IO{
  void printPosition(const Position& pos,
		     std::ostream& f_out = std::cout);
  void printPositionColor(const Position& pos,
			  std::ostream& f_out = std::cout);
  void resetPosition(Position& pos);
  void readPosition(Position& pos,
		    std::string
		    file_name);
  std::string toString(const Position& pos);
  bool readMoveFromKeyboard(std::list<Position>& played_moves,
			    Position& pos,
			    unsigned short int turn);
  std::string diffPositions(const Position& start_pos,
			    const Position& end_pos);
  void Tokenize(const std::string& str,
		std::vector<std::string>& tokens,
		const std::string& delimiters);
}
#endif
