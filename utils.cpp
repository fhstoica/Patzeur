#include <map>
#include "structs.h"
#include "utils.h"
#include "IO.h"

namespace Utils{
#define N 8
  bool checkThreefoldRepetition(const std::list<Position>& played_moves){
    std::map<std::string, unsigned int> repetition_map;
    std::map<std::string, unsigned int>::iterator find_it;
    for(std::list<Position>::const_iterator it = played_moves.begin(); it != played_moves.end(); ++it){
      std::string position_str(IO::toString(*it));
      find_it = repetition_map.find(position_str);
      if(find_it != repetition_map.end()){
	repetition_map[position_str] += 1;
	if(repetition_map[position_str] == 3){
	  std::cout << "Draw by repetition. Position :" << std::endl;
	  IO::printPosition(*it);
	  return(true);
	}
      }
      else{
	repetition_map[position_str] = 1;
      }
    }
    return(false);
  }

  std::string printBitBoard(unsigned long long board){
    std::string s("");
    for(unsigned int i = N; 1 <= i; --i){
      unsigned char rank = board >> ((i-1)*N);
      for(unsigned int j = 0; j < N; ++j){
	unsigned char pos = 1 << j;
	if(rank & pos){
	  s += " 1";
	}
	else{
	  s += " 0";
	}      
      }
      s += "\n";
    }
    return(s);
  }
 
  // unsigned long long genRookAttackTableEntry(unsigned int pos, unsigned long long occ)
  // {
  //   unsigned long long res = 0ULL, init = (unsigned long long)(1ULL) << pos;
  //   for (unsigned long long m = init; res ^= m, m; m >>= 8)
  //     if (occ & m) break;
  //   for (unsigned long long m = init; res ^= m, m; m <<= 8)
  //     if (occ & m) break;
  //   for (unsigned long long m = init; res ^= m, m & 0x7F7F7F7F7F7F7F7Full; m <<= 1)
  //     if (occ & m) break;
  //   for (unsigned long long m = init; res ^= m, m & 0xFEFEFEFEFEFEFEFEull; m >>= 1)
  //     if (occ & m) break;
  //   return res;
  // }

  // unsigned long long genBishopAttackTableEntry(unsigned int pos, unsigned long long occ)
  // {
  //   unsigned long long res = 0ULL, init = (unsigned long long)(1ULL) << pos;
  //   for (unsigned long long m = init; res ^= m, m & 0x7F7F7F7F7F7F7F7Full; m <<= 9)
  //     if (occ & m) break;
  //   for (unsigned long long m = init; res ^= m, m & 0xFEFEFEFEFEFEFEFEull; m <<= 7)
  //     if (occ & m) break;
  //   for (unsigned long long m = init; res ^= m, m & 0xFEFEFEFEFEFEFEFEull; m >>= 9)
  //     if (occ & m) break;
  //   for (unsigned long long m = init; res ^= m, m & 0x7F7F7F7F7F7F7F7Full; m >>= 7)
  //     if (occ & m) break;
  //   return res;
  // }

  void getSetBits(unsigned long long mask, std::vector<unsigned int>& idxs)
  {
    while(mask != 0)
      {
        idxs.push_back(__builtin_ctzll(mask));
        mask &= (mask - 1);
      }
  }

  void genOccupancies(std::vector<unsigned int>& idxs, std::vector<unsigned long long>& occs)
  {
    unsigned int nbits = idxs.size(), nmax = 1 << nbits;
    for (unsigned int i = 0; i < nmax; i++)
      {
        unsigned long long occ = 0;
        for (unsigned int c = 0; c < nbits; c++)
	  if ((i >> c) & 1)
	    occ |= 1ULL << idxs[c];
        occs.push_back(occ);
      }
  }
}
